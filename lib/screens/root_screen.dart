import 'package:flutter/material.dart';
import 'package:kid_learning/services/authentication.dart';
import 'package:kid_learning/common/custom_widget.dart';
import 'package:kid_learning/screens/login_signup_screen.dart';
import 'package:kid_learning/screens/home_screen.dart';
import 'package:kid_learning/common/globals.dart' as globals;
import 'package:animator/animator.dart';

enum AuthStatus {
  NOT_DETERMINED,
  NOT_LOGGED_IN,
  LOGGED_IN,
}

class RootScreen extends StatefulWidget {
  RootScreen({this.auth});
  final BaseAuth auth;

  @override
  State<StatefulWidget> createState() => _RootScreenState();
}

class _RootScreenState extends State<RootScreen> {
  AuthStatus authStatus = AuthStatus.NOT_DETERMINED;
  String _userId = "";

  @override
  void initState() {
    super.initState();
    widget.auth.getCurrentUser().then((user) {
      setState(() {
        if (user != null) {
          _userId = user?.uid;
        }
        authStatus =
            user?.uid == null ? AuthStatus.NOT_LOGGED_IN : AuthStatus.LOGGED_IN;
      });
    });
    print(authStatus);
  }

  void _onLoggedIn() {
    widget.auth.getCurrentUser().then((user) {
      setState(() {
        _userId = user.uid.toString();
      });
    });
    setState(() {
      authStatus = AuthStatus.LOGGED_IN;
    });
  }

  void _onSignedOut() {
    setState(() {
      authStatus = AuthStatus.NOT_LOGGED_IN;
      _userId = "";
    });
  }

  Widget _buildWaitingScreen() {
    return Container(
      alignment: Alignment.center,
      child: CircularProgressIndicator(),
    );
  }

  Widget _renderStartButton() {
    switch (authStatus) {
      case AuthStatus.NOT_DETERMINED:
        return _buildWaitingScreen();
        break;
      case AuthStatus.NOT_LOGGED_IN:
        return GestureDetector(
          onTap: () {
            globals.playBtnSound(1);
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => LoginSignUpScreen(
                          auth: widget.auth,
                          onSignedIn: _onLoggedIn,
                        )));
          },
          child: Container(
            margin: EdgeInsets.only(bottom: 20.0),
            child: Image.asset(
              'images/start-button.png',
              width: 100.0,
              fit: BoxFit.fill,
            ),
          ),
        );
        break;
      case AuthStatus.LOGGED_IN:
        if (_userId.length > 0 && _userId != null) {
          return GestureDetector(
            onTap: () {
              globals.playBtnSound(1);
              globals.userId = _userId;
              globals.auth = widget.auth;
              globals.onSignedOut = _onSignedOut;
              globals.onSignedIn = _onLoggedIn;
              Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => HomeScreen(
                            userId: _userId,
                            auth: widget.auth,
                            onSignedOut: _onSignedOut,
                            onSignedIn: _onLoggedIn,
                          )));
            },
            child: Animator(
              tween: Tween<double>(begin: 0.5, end: 1.0),
              curve: Curves.elasticOut,
              cycles: 0,
              builder: (anim) => Transform.scale(
                  scale: anim.value,
                  child: Container(
                    margin: EdgeInsets.only(bottom: 20.0),
                    child: Image.asset(
                      'images/start-button.png',
                      width: 100.0,
                      fit: BoxFit.fill,
                    ),
                  )),
            ),
          );
        } else
          return _buildWaitingScreen();
        break;
      default:
        return _buildWaitingScreen();
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        MainBackground(),
        Center(
            child: Column(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: 20.0),
              child: Image.asset('images/logo.png'),
            ),
            _renderStartButton(),
          ],
        )),
      ],
    ));
  }
}

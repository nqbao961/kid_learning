import 'package:flutter/material.dart';
import 'package:kid_learning/services/authentication.dart';
import 'package:kid_learning/common/custom_widget.dart';
import 'package:kid_learning/screens/choose_action_screen.dart';
import 'package:kid_learning/common/globals.dart' as globals;
import 'package:kid_learning/screens/achievement_screen.dart';
import 'package:kid_learning/screens/parent/parent_selection_screen.dart';

class HomeScreen extends StatefulWidget {
  HomeScreen(
      {Key key, this.auth, this.userId, this.onSignedOut, this.onSignedIn})
      : super(key: key);

  final BaseAuth auth;
  final VoidCallback onSignedOut;
  final VoidCallback onSignedIn;
  final String userId;

  @override
  State<StatefulWidget> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  bool _isEmailVerified = false;
  String _email = '';
  String _name = '';
  final myController = TextEditingController();

  @override
  void dispose() {
    myController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    globals.playBackgroundMusic();

    widget.auth.getCurrentUser().then((user) {
      setState(() {
        globals.userId = widget.userId;
        _email = user.email;
        _name = user.displayName;
      });
    });

    _checkEmailVerification();
  }

  void _checkEmailVerification() async {
    _isEmailVerified = await widget.auth.isEmailVerified();
    print(_isEmailVerified);
    if (!_isEmailVerified) {
      _showVerifyEmailDialog();
    }
  }

  void _resendVerifyEmail() {
    widget.auth.sendEmailVerification();
    _showVerifyEmailSentDialog();
  }

  void _showVerifyEmailDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text('Xác minh tài khoản'),
            content: Text('Vui lòng kiểm tra email để xác minh tài khoản'),
            actions: <Widget>[
              FlatButton(
                child: Text('Gửi lại liên kết'),
                onPressed: () {
                  Navigator.of(context).pop();
                  _resendVerifyEmail();
                },
              ),
              FlatButton(
                child: Text('Tắt'),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              )
            ],
          );
        });
  }

  void _showVerifyEmailSentDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: new Text('Xác minh tài khoản'),
          content: new Text('Liên kết đã được gửi đến email của bạn'),
          actions: <Widget>[
            new FlatButton(
              child: new Text('Tắt'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  // _signOut() async {
  //   try {
  //     await widget.auth.signOut();
  //     widget.onSignedOut();
  //     Navigator.pushAndRemoveUntil(
  //       context,
  //       MaterialPageRoute(
  //           builder: (context) => LoginSignUpScreen(
  //                 auth: widget.auth,
  //                 onSignedIn: widget.onSignedIn,
  //               )),
  //       ModalRoute.withName('/'),
  //     );
  //   } catch (e) {
  //     print(e);
  //   }
  // }

  // _updateInfoButton() {
  //   Navigator.push(
  //     context,
  //     MaterialPageRoute(
  //       builder: (context) => UpdateInfoScreen(
  //             userId: widget.userId,
  //             auth: widget.auth,
  //           ),
  //     ),
  //   );
  // }

  _parentLock(BuildContext context) {
    return
    Stack(
      alignment: Alignment.center,
      children: <Widget>[
        Center(child: Image.asset('images/dialog-frame.png', width: 450)),
        Container(
          width: 250.0,
          height: 250.0,
          child: Scaffold(
            backgroundColor: Colors.transparent,
            body: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(20, 0, 0, 0),
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(0, 0, 20, 10),
                          child: Image.asset('images/lock-icon.png', width: 30,),
                        ),
                        Container(
                          width: 180.0,
                          child: Text(
                              'Xin cho biết kết quả',
                              style: TextStyle(fontSize: 18.0)
                          ),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    child: Padding(
                      padding: const EdgeInsets.fromLTRB(0.0, 20.0, 0.0, 20.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            width: 100,
                            height: 40,
                            color: Colors.black54,
                            child: Center(
                                  child:
                                  Text('4 + 8', style: TextStyle(fontSize: 18.0, color: Colors.white)),
                                ),
                          ),
                          Padding(
                            padding: const EdgeInsets.fromLTRB(7, 0, 7, 0),
                            child: Center(
                              child: Text('=', style: TextStyle(fontSize: 18, color: Colors.black),),
                            ),
                          ),
                          Container(
                              width: 40,
                              height: 40,
                              color: Colors.black54,
                              child: Center(
                                child:
                                Text('', style: TextStyle(fontSize: 18.0, color: Colors.white)),
                              ),
                            ),

                        ],
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pop();
                      globals.playBtnSound(1);
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ParentSelectionScreen(),
                        ),
                      );
                    },
                    child: Stack(
                      alignment: AlignmentDirectional.center,
                      children: <Widget>[
                        Image.asset(
                          'images/button-blue.png',
                          width: 160.0,
                        ),
                        Text(
                          'Mở khóa',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white, fontSize: 16.0),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            )
          ),
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      //endDrawer: SettingDrawer(),
      body: Stack(
        children: <Widget>[
          MainBackground(),
          CommonButton(),
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Image.asset(
                    'images/stitch.gif',
                    width: 150.0,
                  ),
                  GestureDetector(
                    onTap: () {
                      globals.playBtnSound(1);
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => AchievementScreen(),
                        ),
                      );
                    },
                    child: Image.asset(
                      'images/gold-cup.gif',
                      width: 130.0,
                    ),
                  )
                ],
              ),
            ],
          ),
          Center(
            child: Image.asset(
              'images/kid-frame.png',
              width: 350.0,
            ),
          ),
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Chào bé $_name!',
                  style: TextStyle(fontSize: 18.0),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: GestureDetector(
                    onTap: () {
                      globals.playBtnSound(1);
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => ChooseActionScreen(
                                userId: widget.userId,
                              ),
                        ),
                      );
                    },
                    child: Stack(
                      alignment: AlignmentDirectional.center,
                      children: <Widget>[
                        Image.asset(
                          'images/button-green.png',
                          width: 200.0,
                        ),
                        Text(
                          'Bắt đầu học',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white, fontSize: 20.0),
                        ),
                      ],
                    ),
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    showDialog(
                        barrierDismissible: true,
                        context: context,
                        builder: (context) => _parentLock(context));
                  },
                  child: Stack(
                    alignment: AlignmentDirectional.center,
                    children: <Widget>[
                      Image.asset(
                        'images/button-blue.png',
                        width: 160.0,
                      ),
                      Text(
                        'Phụ huynh',
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white, fontSize: 16.0),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

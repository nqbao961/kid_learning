import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kid_learning/common/custom_widget.dart';
import 'package:kid_learning/common/globals.dart' as globals;
import 'package:kid_learning/screens/exam/do_examination.dart';

class LevelExamButton extends StatelessWidget {
  final String text, image, level;

  LevelExamButton(this.text, this.image, this.level);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        globals.playBtnSound(2);
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => DoExamination(
                  level: level,
                ),
          ),
        );
      },
      child: Stack(
        alignment: AlignmentDirectional.center,
        children: <Widget>[
          Image.asset(
            image,
          ),
          Text(
            text,
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white, fontSize: 20.0),
          ),
        ],
      ),
    );
  }
}

class LevelExamination extends StatelessWidget {
  LevelExamination({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        MainBackground(),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 300.0,
              margin: EdgeInsets.only(top: 30.0, bottom: 30.0),
              child: Stack(
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      LevelExamButton('Dễ', 'images/button-easy.png', 'easy'),
                      LevelExamButton(
                          'Trung bình', 'images/button-normal.png', 'normal'),
                      LevelExamButton(
                          'Nâng cao', 'images/button-hard.png', 'hard'),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
        CommonButton(),
      ],
    ));
  }
}
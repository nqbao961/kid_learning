import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:kid_learning/common/custom_widget.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:audioplayers/audio_cache.dart';
import "dart:math";
import 'package:kid_learning/common/globals.dart' as globals;
import 'package:cloud_functions/cloud_functions.dart';
import 'package:youtube_player/youtube_player.dart';
import 'package:flutter/services.dart';

class DoExamination extends StatefulWidget {
  final String level;

  DoExamination({Key key, this.level}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _DoExaminationState();
}

class _DoExaminationState extends State<DoExamination> {
  _DoExaminationState();

  var mainSlider;
  List<Widget> _sliderItems = [];
  List<String> _correctStatement = [
    'Đúng rồi. Bé giỏi quá!',
    'Chính xác. Thật tuyệt vời!',
    'Chúc mừng bé. Đáp án chính xác!'
  ];

  FlutterTts flutterTts;
  String language = 'en-US';

  AudioPlayer audioPlayer = AudioPlayer();
  AudioCache audioCache = new AudioCache();

  List quizList;
  int score = 0;
  int highScore = 0;
  int quizNumber = 0;

  int _current = 0;
  int _quizCount = 0;

  final AsyncMemoizer _memoizer = AsyncMemoizer();

  final _formKey = GlobalKey<FormState>();
  var _answerInput = new TextEditingController();

  @override
  initState() {
    Firestore.instance
        .document('users/${globals.userId}')
        .get()
        .then((DocumentSnapshot ds) {
      setState(() {
        highScore = ds.data['high_score'] == null
            ? 0
            : ds.data['high_score'][widget.level] == null
                ? 0
                : ds.data['high_score'][widget.level];
      });
    });
    super.initState();
    initTts();
  }

  initTts() {
    flutterTts = FlutterTts();
    flutterTts.setLanguage(language);
    flutterTts.setVoice(globals.voiceType);
    flutterTts.setVolume(globals.volume);
  }

  void genQuizList(quiz) {
    _quizCount++;
    _sliderItems.add(StreamBuilder<DocumentSnapshot>(
      stream: quiz.snapshots(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        print(snapshot.hasData);
        if (!snapshot.hasData) return new Text('Loading...');
        String questionContent = snapshot.data['question']['content'];
        String questionImage = snapshot.data['question']['image'];
        String questionPronounce = snapshot.data['question']['pronounce'];
        String choiceA = snapshot.data['choices'][0];
        String choiceB = snapshot.data['choices'][1];
        String choiceC = snapshot.data['choices'][2];
        String choiceD = snapshot.data['choices'][3];
        String answer = snapshot.data['answer_fill_blank'] == ""
            ? snapshot.data['choices'][snapshot.data['answer_multiple_choice']]
            : snapshot.data['answer_fill_blank'];
        String type = snapshot.data['type'];
        String quizId = snapshot.data.documentID;
        Firestore.instance
            .document('quizzes/$quizId')
            .get()
            .then((DocumentSnapshot ds) {
          if (!ds.data['results'].containsKey(globals.userId)) {
            Firestore.instance.document('quizzes/$quizId').setData({
              'results': {
                globals.userId: {'correct_num': 0, 'incorrect_num': 0}
              }
            }, merge: true);
          }
        });
        if (type == 'multiple-choice') {
          flutterTts.speak(questionPronounce);
          return Container(
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(color: Colors.white),
              child: Column(
                children: <Widget>[
                  Expanded(
                    flex: 2,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        questionContent != ''
                            ? Expanded(
                                flex: 1,
                                child: Center(
                                  child: Text(
                                    questionContent,
                                    style: TextStyle(fontSize: 20.0),
                                  ),
                                ),
                              )
                            : Container(),
                        questionImage != ''
                            ? Expanded(
                                flex: 2,
                                child: GestureDetector(
                                  onTap: () {
                                    flutterTts.speak(questionPronounce);
                                  },
                                  child: FadeInImage.assetNetwork(
                                    placeholder: 'images/loading2.gif',
                                    image: questionImage,
                                    fit: BoxFit.contain,
                                  ),
                                ))
                            : questionPronounce != ''
                                ? GestureDetector(
                                    onTap: () {
                                      flutterTts.speak(questionPronounce);
                                    },
                                    child: Icon(
                                      Icons.volume_up,
                                      size: 100.0,
                                    ),
                                  )
                                : Container(),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Column(
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            choiceSelector('A.', choiceA, answer, quizId),
                            choiceSelector('B.', choiceB, answer, quizId)
                          ],
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Column(
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            choiceSelector('C.', choiceC, answer, quizId),
                            choiceSelector('D.', choiceD, answer, quizId)
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ));
        } else if (type == 'fill-blank') {
          flutterTts.speak(questionPronounce);
          return Container(
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(color: Colors.white),
              child: ListView(
                shrinkWrap: true,
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height / 5.0,
                    child: Center(
                      child: Text(
                        questionContent,
                        style: TextStyle(fontSize: 20.0),
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height / 2.0,
                    child: GestureDetector(
                      onTap: () {
                        flutterTts.speak(questionPronounce);
                      },
                      child: questionImage != ''
                          ? FadeInImage.assetNetwork(
                              placeholder: 'images/loading2.gif',
                              image: questionImage)
                          : Icon(
                              Icons.volume_up,
                              size: 200.0,
                            ),
                    ),
                  ),
                  Container(
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          Container(
                            width: 200.0,
                            child: TextFormField(
                              controller: _answerInput,
                              maxLines: 1,
                              textAlign: TextAlign.center,
                              keyboardType: TextInputType.text,
                              autofocus: false,
                              onSaved: (value) =>
                                  _answerInput.text = value.toLowerCase(),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: GestureDetector(
                              onTap: () {
                                globals.playBtnSound(1);
                                SystemChrome.setEnabledSystemUIOverlays([]);
                                FocusScope.of(context)
                                    .requestFocus(new FocusNode());
                                final form = _formKey.currentState;
                                form.save();
                                checkAnswerInput(answer, quizId);
                              },
                              child: Stack(
                                alignment: AlignmentDirectional.center,
                                children: <Widget>[
                                  Image.asset(
                                    'images/button-blue.png',
                                    width: 160.0,
                                  ),
                                  Text(
                                    'Check',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 16.0),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ));
        }
      },
    ));
  }

  void genCompletedSlide(List quizReference) {
    _sliderItems.add(Builder(
      builder: (BuildContext context) {
        if (score > highScore) {
          Firestore.instance.document('users/${globals.userId}').setData({
            'high_score': {widget.level: score}
          }, merge: true);
        }
        audioCache.play('sounds/tada_sound.wav', volume: globals.volume);
        return Container(
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(color: Colors.white),
          child: GestureDetector(
            onTap: () {
              Navigator.of(context).pop();
              globals.resumeBackgroundMusic();
            },
            child: Stack(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: Image.asset(
                    'images/star-background.gif',
                    repeat: ImageRepeat.repeatX,
                  ),
                ),
                Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Hoàn thành!',
                        style: TextStyle(fontSize: 40.0, color: Colors.yellow),
                      ),
                      Text(
                        'Số điểm của bé là: $score/${quizReference.length}',
                        style: TextStyle(fontSize: 45.0, color: Colors.yellow),
                      ),
                      Stack(
                        alignment: AlignmentDirectional.center,
                        children: <Widget>[
                          Image.asset(
                            'images/button-red.png',
                            width: 150.0,
                          ),
                          Text(
                            'Thoát',
                            textAlign: TextAlign.center,
                            style:
                                TextStyle(color: Colors.white, fontSize: 16.0),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        );
      },
    ));
  }

  void checkAnswer(choice, answer, quizId) {
    final DocumentReference postRef =
        Firestore.instance.document('quizzes/$quizId');
    Firestore.instance.runTransaction((Transaction tx) async {
      DocumentSnapshot postSnapshot = await tx.get(postRef);
      if (postSnapshot.exists) {
        var curCorrectNum =
            postSnapshot.data['results'][globals.userId]['correct_num'];
        var curIncorrectNum =
            postSnapshot.data['results'][globals.userId]['incorrect_num'];
        var curTotalCorrectNum =
            postSnapshot.data['total_result']['correct_num'];
        var curTotalIncorrectNum =
            postSnapshot.data['total_result']['incorrect_num'];
        Map allResults = postSnapshot.data['results'];
        Map userResults = choice == answer
            ? {
                'correct_num': curCorrectNum == null ? 0 : curCorrectNum + 1,
                'incorrect_num': curIncorrectNum == null ? 0 : curIncorrectNum
              }
            : {
                'correct_num': curCorrectNum == null ? 0 : curCorrectNum,
                'incorrect_num':
                    curIncorrectNum == null ? 0 : curIncorrectNum + 1
              };
        Map newTotalResult = choice == answer
            ? {
                'correct_num': curTotalCorrectNum + 1,
                'incorrect_num': curTotalIncorrectNum
              }
            : {
                'correct_num': curTotalCorrectNum,
                'incorrect_num': curTotalIncorrectNum + 1
              };
        allResults.update(globals.userId, (val) => userResults);
        await tx
            .update(postRef, <String, dynamic>{'total_result': newTotalResult});
        await tx.update(postRef, <String, dynamic>{'results': allResults});
      }
    });
    if (choice == answer) {
      setState(() {
        score++;
      });
    }
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        Future.delayed(Duration(milliseconds: 2500), () {
          Navigator.of(context).pop(true);
          mainSlider.nextPage(
              duration: Duration(milliseconds: 300), curve: Curves.linear);
        });

        choice == answer
            ? audioCache.play('sounds/correct.mp3', volume: globals.volume)
            : audioCache.play('sounds/incorrect.mp3', volume: globals.volume);
        return AlertDialog(
          content: choice == answer
              ? Text(
                  _correctStatement[Random().nextInt(_correctStatement.length)],
                  style: TextStyle(color: Colors.green, fontSize: 22.0))
              : Text('Sai mất rồi!',
                  style: TextStyle(color: Colors.red, fontSize: 22.0)),
        );
      },
    );
  }

  void checkAnswerInput(answer, quizId) {
    final DocumentReference postRef =
        Firestore.instance.document('quizzes/$quizId');
    Firestore.instance.runTransaction((Transaction tx) async {
      DocumentSnapshot postSnapshot = await tx.get(postRef);
      if (postSnapshot.exists) {
        var curCorrectNum =
            postSnapshot.data['results'][globals.userId]['correct_num'];
        var curIncorrectNum =
            postSnapshot.data['results'][globals.userId]['incorrect_num'];
        var curTotalCorrectNum =
            postSnapshot.data['total_result']['correct_num'];
        var curTotalIncorrectNum =
            postSnapshot.data['total_result']['incorrect_num'];
        Map allResults = postSnapshot.data['results'];
        Map userResults = _answerInput.text == answer
            ? {
                'correct_num': curCorrectNum == null ? 0 : curCorrectNum + 1,
                'incorrect_num': curIncorrectNum == null ? 0 : curIncorrectNum
              }
            : {
                'correct_num': curCorrectNum == null ? 0 : curCorrectNum,
                'incorrect_num':
                    curIncorrectNum == null ? 0 : curIncorrectNum + 1
              };
        Map newTotalResult = _answerInput.text == answer
            ? {
                'correct_num': curTotalCorrectNum + 1,
                'incorrect_num': curTotalIncorrectNum
              }
            : {
                'correct_num': curTotalCorrectNum,
                'incorrect_num': curTotalIncorrectNum + 1
              };
        allResults.update(globals.userId, (val) => userResults);
        await tx
            .update(postRef, <String, dynamic>{'total_result': newTotalResult});
        await tx.update(postRef, <String, dynamic>{'results': allResults});
      }
    });
    if (_answerInput.text == answer) {
      setState(() {
        score++;
      });
    }
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        _answerInput.text == answer
            ? audioCache.play('sounds/correct.mp3', volume: globals.volume)
            : audioCache.play('sounds/incorrect.mp3', volume: globals.volume);
        return AlertDialog(
          content: _answerInput.text == answer
              ? Text(
                  _correctStatement[Random().nextInt(_correctStatement.length)],
                  style: TextStyle(color: Colors.green, fontSize: 22.0))
              : Text('Sai mất rồi!',
                  style: TextStyle(color: Colors.red, fontSize: 22.0)),
        );
      },
    );
    Future.delayed(Duration(milliseconds: 2500), () {
      Navigator.of(context).pop(true);
      mainSlider.nextPage(
          duration: Duration(milliseconds: 300), curve: Curves.linear);
    });
  }

  Widget choiceSelector(index, choice, answer, quizId) {
    return GestureDetector(
      onTap: () {
        globals.playBtnSound(2);
        checkAnswer(choice, answer, quizId);
      },
      child: Stack(
        alignment: AlignmentDirectional.center,
        children: <Widget>[
          Image.asset(
            'images/button-blue.png',
            width: 160.0,
          ),
          Text(
            '$index $choice',
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white, fontSize: 16.0),
          ),
        ],
      ),
    );
  }

  _quizFuture() {
    return _memoizer.runOnce(() async {
      try {
        await CloudFunctions.instance.call(
          functionName: 'getRecommendedQuizList',
          parameters: <String, dynamic>{
            'level': widget.level,
//          'topic': 'animals',
            'userId': globals.userId
          },
        );
      } on CloudFunctionsException catch (e) {
        print('caught firebase functions exception');
        print(e.code);
        print(e.message);
        print(e.details);
      } catch (e) {
        print('caught generic exception');
        print(e);
      }
      return Firestore.instance
          .collection('users')
          .document(globals.userId)
          .get();
    });
  }

  Future<bool> _onBackPress() {
    return showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: Text('Bạn có muốn thoát bài kiểm tra không?'),
              actions: <Widget>[
                FlatButton(
                  child: Text(
                    'Có',
                    style: TextStyle(color: Colors.red, fontSize: 20.0),
                  ),
                  onPressed: () {
                    Navigator.pop(context, true);
                    Navigator.pop(context, true);
                    globals.resumeBackgroundMusic();
                  },
                ),
                FlatButton(
                  child: Text(
                    'Không',
                    style: TextStyle(color: Colors.green, fontSize: 20.0),
                  ),
                  onPressed: () {
                    Navigator.pop(context, true);
                  },
                ),
              ],
            ));
  }

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }

    return result;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        _onBackPress();
      },
      child: Scaffold(
        body: Stack(
          children: <Widget>[
            MainBackground(),
            FutureBuilder(
              future: _quizFuture(),
              builder: (BuildContext context, testSnapshot) {
                globals.pauseBackgroundMusic();
                if (!testSnapshot.hasData)
                  return Center(
                    child: Image.asset('images/loading2.gif', width: 100.0),
                  );
                List quizReference = testSnapshot.data['recommended_quiz_list'];
                if (_quizCount != quizReference.length) {
                  quizReference.forEach((quiz) => genQuizList(quiz));
                  genCompletedSlide(quizReference);
                }
                mainSlider = CarouselSlider(
                  enableInfiniteScroll: false,
                  enableScroll: false,
                  viewportFraction: 1.1,
                  autoPlay: false,
                  height: 400.0,
                  items: _sliderItems,
                  onPageChanged: (index) {
                    _answerInput.clear();
                    setState(() {
                      _current = index;
                    });
                  },
                );
                return Stack(
                  children: <Widget>[
                    mainSlider,
                    Container(
                      alignment: Alignment.bottomCenter,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: map<Widget>(
                          _sliderItems,
                          (index, url) {
                            return Container(
                              width: 8.0,
                              height: 8.0,
                              margin: EdgeInsets.symmetric(
                                  vertical: 10.0, horizontal: 2.0),
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  color: _current == index
                                      ? Color.fromRGBO(0, 0, 0, 0.9)
                                      : Color.fromRGBO(0, 0, 0, 0.4)),
                            );
                          },
                        ),
                      ),
                    ),
                  ],
                );
              },
            ),
            Container(
              alignment: Alignment.topRight,
              child: GestureDetector(
                onTap: () {
                  _onBackPress();
                },
                child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Image.asset('images/uncheck-icon.png', width: 35.0),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

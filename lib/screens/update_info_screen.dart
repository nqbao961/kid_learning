import 'package:flutter/material.dart';
import 'package:kid_learning/common/custom_widget.dart';
import 'package:kid_learning/services/authentication.dart';
import 'package:flutter/services.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:date_format/date_format.dart';
import 'dart:async';

class UpdateInfoScreen extends StatefulWidget {
  UpdateInfoScreen({Key key, this.auth, this.userId}) : super(key: key);

  final BaseAuth auth;
  final String userId;

  @override
  State<StatefulWidget> createState() => _UpdateInfoScreenState();
}

class _UpdateInfoScreenState extends State<UpdateInfoScreen> {
  final _formKey = GlobalKey<FormState>();
  //String _name;
  var _name = new TextEditingController();
  var _displayBirthday = new TextEditingController();
  String _birthday = '';
  String _sex;
  var _address = new TextEditingController();
  var _school = new TextEditingController();
  var _password = new TextEditingController();

  void _handleSexRadio(String value) {
    setState(() {
      _sex = value;
    });
  }

  @override
  void initState() {
    super.initState();
    widget.auth.getCurrentUser().then((user) {
      setState(() {
        _name.text = user.displayName;
      });
    });
    Firestore.instance
        .collection('users')
        .document(widget.userId)
        .snapshots()
        .listen((data) => setState(() {
              data['birthday'] == null
                  ? _birthday = ''
                  : _birthday = data['birthday'];
              _birthday == ''
                  ? _displayBirthday.text = ''
                  : _displayBirthday.text = formatDate(
                      DateTime.parse(_birthday), [dd, '/', mm, '/', yyyy]);
              _sex = data['sex'];
              _address.text = data['address'];
              _school.text = data['school'];
            }));
  }

  bool _validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void _validateAndSubmit() async {
    if (_validateAndSave()) {
      try {
        updateName();
        updateData();
        if (_password.text != '') {
          updatePassword();
        }
      } catch (e) {
        print('Error: $e');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          MainBackground(),
          Stack(
            children: <Widget>[
              _showBody(),
              CommonButton(),
            ],
          )
        ],
      ),
    );
  }

  Widget _showBody() {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: GestureDetector(
        onTap: () {
          SystemChrome.setEnabledSystemUIOverlays([]);
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Container(
          color: Color.fromRGBO(255, 255, 255, 0.5),
          padding: EdgeInsets.all(16.0),
          child: Form(
            key: _formKey,
            child: ListView(
              shrinkWrap: true,
              children: <Widget>[
                _showNameInput(),
                _showBirthdayInput(),
                _showSexInput(),
                _showAddressInput(),
                _showSchoolInput(),
                _showPasswordInput(),
                _showUpdateButton(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _showNameInput() {
    return TextFormField(
      controller: _name,
      maxLines: 1,
      keyboardType: TextInputType.text,
      autofocus: false,
      decoration: InputDecoration(
          hintText: 'Tên',
          icon: Icon(
            Icons.account_box,
            color: Colors.green,
          ),
          labelText: 'Tên',
          labelStyle: TextStyle(fontSize: 20.0)),
      onSaved: (value) => _name.text = value,
    );
  }

  Widget _showBirthdayInput() {
    return InkWell(
      onTap: () {
        _selectDate();
      },
      child: IgnorePointer(
        child: TextFormField(
          controller: _displayBirthday,
          maxLines: 1,
          keyboardType: TextInputType.text,
          autofocus: false,
          decoration: InputDecoration(
              hintText: 'Ngày sinh',
              icon: Icon(
                Icons.date_range,
                color: Colors.green,
              ),
              labelText: 'Ngày sinh',
              labelStyle: TextStyle(fontSize: 20.0)),
          //onSaved: (value) => _birthday = value,
        ),
      ),
    );
  }

  Widget _showSexInput() {
    return Row(
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(right: 16.0),
          child: Icon(
            Icons.face,
            color: Colors.green,
          ),
        ),
        Text('Giới tính', style: TextStyle(fontSize: 15.0, color: Color.fromRGBO(0,0,0,0.54)),),
        Radio(
          value: 'male',
          groupValue: _sex,
          onChanged: _handleSexRadio,
        ),
        Text('Nam'),
        Radio(
          value: 'female',
          groupValue: _sex,
          onChanged: _handleSexRadio,
        ),
        Text('Nữ'),
      ],
    );
  }

  Widget _showAddressInput() {
    return TextFormField(
      controller: _address,
      maxLines: 1,
      keyboardType: TextInputType.text,
      autofocus: false,
      decoration: InputDecoration(
          hintText: 'Địa chỉ',
          icon: Icon(
            Icons.place,
            color: Colors.green,
          ),
          labelText: 'Địa chỉ',
          labelStyle: TextStyle(fontSize: 20.0)),
      onSaved: (value) => _address.text = value,
    );
  }

  Widget _showSchoolInput() {
    return TextFormField(
      controller: _school,
      maxLines: 1,
      keyboardType: TextInputType.text,
      autofocus: false,
      decoration: InputDecoration(
          hintText: 'Trường học',
          icon: Icon(
            Icons.school,
            color: Colors.green,
          ),
          labelText: 'Trường học',
          labelStyle: TextStyle(fontSize: 20.0)),
      onSaved: (value) => _school.text = value,
    );
  }

  Widget _showPasswordInput() {
    return TextFormField(
      obscureText: true,
      controller: _password,
      maxLines: 1,
      keyboardType: TextInputType.text,
      autofocus: false,
      decoration: InputDecoration(
          hintText: 'Mật khẩu',
          icon: Icon(
            Icons.lock,
            color: Colors.green,
          ),
          labelText: 'Mật khẩu',
          labelStyle: TextStyle(fontSize: 20.0)),
      onSaved: (value) => _password.text = value,
    );
  }

  Widget _showUpdateButton() {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: GestureDetector(
        child: Stack(
          alignment: AlignmentDirectional.center,
          children: <Widget>[
            Image.asset(
              'images/button-blue.png',
              width: 160.0,
            ),
            Text('Cập nhật', style: TextStyle(color: Colors.white)),
          ],
        ),
        onTap: () {
          SystemChrome.setEnabledSystemUIOverlays([]);
          FocusScope.of(context).requestFocus(new FocusNode());
          _validateAndSubmit();
          Navigator.pop(context, true);
        },
      ),
    );
  }

  void updateName() {
    widget.auth.getCurrentUser().then((user) {
      UserUpdateInfo userUpdateInfo = UserUpdateInfo();
      userUpdateInfo.displayName = _name.text;
      user.updateProfile(userUpdateInfo);
    });
  }

  void updateData() {
    Firestore.instance.collection('users').document(widget.userId).setData({
      'birthday': _birthday,
      'sex': _sex,
      'address': _address.text,
      'school': _school.text
    }, merge: true);
  }

  void updatePassword() {
    widget.auth.updatePassword(_password.text);
  }

  Future _selectDate() async {
    DateTime picked = await showDatePicker(
        context: context,
        initialDate:
            _birthday == '' ? DateTime.now() : DateTime.parse(_birthday),
        firstDate: DateTime(1900),
        lastDate: DateTime.now());
    if (picked != null) {
      setState(() {
        _displayBirthday.text = formatDate(picked, [dd, '/', mm, '/', yyyy]);
        _birthday = formatDate(picked, [yyyy, '-', mm, '-', dd]);
      });
    }
  }
}

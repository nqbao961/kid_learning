import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kid_learning/common/custom_widget.dart';
import 'package:kid_learning/common/globals.dart' as globals;
import 'package:kid_learning/screens/exam/level_examination.dart';
import 'package:kid_learning/screens/entertainment/game_selection_screen.dart';
import 'package:kid_learning/screens/entertainment/video_selection_screen.dart';
import 'package:kid_learning/screens/lesson/choose_level_screen.dart';

class ChooseActionScreen extends StatelessWidget {
  final String userId;

  ChooseActionScreen({Key key, @required this.userId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        MainBackground(),
        Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                GestureDetector(
                  onTap: () {
                    globals.playBtnSound(2);
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => LevelExamination(),
                      ),
                    );
                  },
                  child: Image.asset(
                    'images/examination.gif',
                    width: 200.0,
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    globals.playBtnSound(2);
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => ChooseLevelScreen(
                              userId: globals.userId,
                            ),
                      ),
                    );
                  },
                  child: Image.asset(
                    'images/learning.gif',
                    width: 200.0,
                  ),
                ),
                GestureDetector(
                  onTap: () {
                    globals.playBtnSound(2);
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => VideoSelectionScreen(),
                      ),
                    );
                  },
                  child: Image.asset(
                    'images/game.gif',
                    width: 200.0,
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              children: <Widget>[
                Text(
                  'Kiểm tra',
                  style: TextStyle(fontSize: 25.0, color: Colors.red, shadows: [
                    Shadow(
                        // bottomLeft
                        offset: Offset(-1.5, -1.5),
                        color: Colors.white),
                    Shadow(
                        // bottomRight
                        offset: Offset(1.5, -1.5),
                        color: Colors.white),
                    Shadow(
                        // topRight
                        offset: Offset(1.5, 1.5),
                        color: Colors.white),
                    Shadow(
                        // topLeft
                        offset: Offset(-1.5, 1.5),
                        color: Colors.white),
                  ]),
                ),
                Text(
                  'Học bài',
                  style:
                      TextStyle(fontSize: 25.0, color: Colors.blue, shadows: [
                    Shadow(
                        offset: Offset(-1.5, -1.5),
                        color: Colors.white),
                    Shadow(
                        offset: Offset(1.5, -1.5),
                        color: Colors.white),
                    Shadow(
                        offset: Offset(1.5, 1.5),
                        color: Colors.white),
                    Shadow(
                        offset: Offset(-1.5, 1.5),
                        color: Colors.white),
                  ]),
                ),
                Text(
                  'Giải trí',
                  style:
                      TextStyle(fontSize: 25.0, color: Colors.purple, shadows: [
                    Shadow(
                        offset: Offset(-1.5, -1.5),
                        color: Colors.white),
                    Shadow(
                        offset: Offset(1.5, -1.5),
                        color: Colors.white),
                    Shadow(
                        offset: Offset(1.5, 1.5),
                        color: Colors.white),
                    Shadow(
                        offset: Offset(-1.5, 1.5),
                        color: Colors.white),
                  ]),
                ),
              ],
            ),
          ],
        ),
        CommonButton(),
      ],
    ));
  }
}

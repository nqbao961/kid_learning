import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kid_learning/services/authentication.dart';
import 'package:kid_learning/common/custom_widget.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:kid_learning/common/globals.dart' as globals;
import 'package:toast/toast.dart';

enum FormMode { LOGIN, SIGNUP }

class LoginSignUpScreen extends StatefulWidget {
  LoginSignUpScreen({this.auth, this.onSignedIn});

  final BaseAuth auth;
  final VoidCallback onSignedIn;

  @override
  State<StatefulWidget> createState() => _LoginSignUpScreenState();
}

class _LoginSignUpScreenState extends State<LoginSignUpScreen> {
  final _formKey = GlobalKey<FormState>();
  final _formKey2 = GlobalKey<FormState>();
  bool _isLoading;
  String _email;
  String _password;
  String _name;
  String _errorMessage;
  FormMode _formMode = FormMode.LOGIN;

  bool _validateAndSave() {
    final form = _formKey.currentState;
    if (form.validate()) {
      form.save();
      return true;
    }
    return false;
  }

  void _validateAndSubmit() async {
    setState(() {
      _errorMessage = '';
      print(_email);
      if (_email == null || _name == null || _password == null) {
        _isLoading = false;
      } else {
        _isLoading = true;
      }
    });
    if (_validateAndSave()) {
      String userId = '';
      try {
        if (_formMode == FormMode.LOGIN) {
          userId = await widget.auth.signIn(_email, _password);
          print('Signed in: $userId');
          globals.userId = userId;
          globals.email = _email;
        } else {
          userId = await widget.auth.signUp(_email, _password);
          print('Signed up user: $userId');
          widget.auth.sendEmailVerification();
          _showCreatedDialog();
          userId = await widget.auth.signIn(_email, _password);
          print('Signed in: $userId');
          globals.userId = userId;
          globals.email = _email;
          widget.auth.addUserDoc();
          widget.auth.getCurrentUser().then((user) {
            UserUpdateInfo userUpdateInfo = UserUpdateInfo();
            userUpdateInfo.displayName = _name;
            user.updateProfile(userUpdateInfo);
          });
          Navigator.of(context).pop();
        }
        setState(() {
          _isLoading = false;
        });

        if (userId.length > 0 && userId != null) {
          widget.onSignedIn();
          Navigator.of(context).pop();
        }
      } catch (e) {
        print('Error: $e');
        setState(() {
          _isLoading = false;
          _errorMessage = e.message;
        });
      }
    }
  }

  @override
  void initState() {
    _errorMessage = "";
    _isLoading = false;
    super.initState();
  }

  void _showCreatedDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          content: Text('Tài khoản đã được tạo thành công. Đang đăng nhập...'),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          MainBackground(),
          Stack(
            children: <Widget>[
              _showBody(),
              _showCircularProgress(),
            ],
          )
        ],
      ),
    );
  }

  Widget _showCircularProgress() {
    if (_isLoading) {
      return Center(
        child: CircularProgressIndicator(),
      );
    }
    return Container(
      height: 0.0,
      width: 0.0,
    );
  }

  Widget _showEmailInput() {
    return TextFormField(
      maxLines: 1,
      keyboardType: TextInputType.emailAddress,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Email',
        icon: Icon(
          Icons.mail,
          color: Colors.green,
        ),
      ),
      validator: (value) => value.isEmpty ? 'Vui lòng nhập Email' : null,
      onSaved: (value) => _email = value,
    );
  }

  Widget _showNameInput() {
    return _formMode == FormMode.SIGNUP
        ? TextFormField(
            maxLines: 1,
            keyboardType: TextInputType.text,
            autofocus: false,
            decoration: InputDecoration(
              hintText: 'Name',
              icon: Icon(
                Icons.account_box,
                color: Colors.green,
              ),
            ),
            validator: (value) => value.isEmpty ? 'Vui lòng nhập tên' : null,
            onSaved: (value) => _name = value,
          )
        : Container(
            height: 0.0,
          );
  }

  Widget _showPasswordInput() {
    return TextFormField(
      maxLines: 1,
      obscureText: true,
      autofocus: false,
      decoration: InputDecoration(
        hintText: 'Mật khẩu',
        icon: Icon(
          Icons.lock,
          color: Colors.green,
        ),
      ),
      validator: (value) => value.isEmpty ? 'Vui lòng nhập mật khẩu' : null,
      onSaved: (value) => _password = value,
    );
  }

  Widget _showPrimaryButton() {
    return Padding(
      padding: const EdgeInsets.only(top: 8.0),
      child: GestureDetector(
        child: Stack(
          alignment: AlignmentDirectional.center,
          children: <Widget>[
            Image.asset(
              'images/button-blue.png',
              width: 160.0,
            ),
            _formMode == FormMode.LOGIN
                ? Text('Đăng nhập', style: TextStyle(color: Colors.white))
                : Text('Tạo tài khoản', style: TextStyle(color: Colors.white)),
          ],
        ),
        onTap: () {
          globals.playBtnSound(1);
          SystemChrome.setEnabledSystemUIOverlays([]);
          FocusScope.of(context).requestFocus(new FocusNode());
          _validateAndSubmit();
        },
      ),
    );
  }

  Widget _showSecondaryButton() {
    return FlatButton(
      child: _formMode == FormMode.LOGIN
          ? Text('Tạo tài khoản')
          : Text('Đã có tài khoản? Đăng nhập'),
      onPressed: _formMode == FormMode.LOGIN
          ? _changeFormToSignUp
          : _changeFormToLogin,
    );
  }

  Widget _showForgotButton() {
    return FlatButton(
      child: Text('Quên mật khẩu?'),
      onPressed: () {
        showForgotDialog();
      },
    );
  }

  void showForgotDialog() {
    showDialog(
        context: context,
        builder: (BuildContext context) {
          return SimpleDialog(
            children: <Widget>[
              Container(
                width: 400.0,
                height: 150.0,
                padding: EdgeInsets.all(16.0),
                child: Form(
                  key: _formKey2,
                  child: ListView(
                    shrinkWrap: true,
                    children: <Widget>[
                      TextFormField(
                        maxLines: 1,
                        keyboardType: TextInputType.emailAddress,
                        autofocus: false,
                        decoration: InputDecoration(
                          hintText: 'Email',
                          icon: Icon(
                            Icons.mail,
                            color: Colors.green,
                          ),
                        ),
                        validator: (value) =>
                            value.isEmpty ? 'Vui lòng nhập Email' : null,
                        onSaved: (value) => _email = value,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 8.0),
                        child: GestureDetector(
                          child: Stack(
                            alignment: AlignmentDirectional.center,
                            children: <Widget>[
                              Image.asset(
                                'images/button-blue.png',
                                width: 160.0,
                              ),
                              Text('Gửi link reset',
                                  style: TextStyle(color: Colors.white)),
                            ],
                          ),
                          onTap: () {
                            globals.playBtnSound(1);
                            SystemChrome.setEnabledSystemUIOverlays([]);
                            FocusScope.of(context)
                                .requestFocus(new FocusNode());
                            final form2 = _formKey2.currentState;
                            if (form2.validate()) {
                              form2.save();
                              widget.auth.sendPasswordResetEmail(_email);
                              Toast.show("Đã gửi link, vui lòng kiểm tra email.",
                                  context,
                                  duration: Toast.LENGTH_LONG,
                                  gravity: Toast.BOTTOM);
                            }
                          },
                        ),
                      )
                    ],
                  ),
                ),
              )
            ],
          );
        });
  }

  void _changeFormToSignUp() {
    _formKey.currentState.reset();
    _errorMessage = '';
    setState(() {
      _formMode = FormMode.SIGNUP;
    });
  }

  void _changeFormToLogin() {
    _formKey.currentState.reset();
    _errorMessage = '';
    setState(() {
      _formMode = FormMode.LOGIN;
    });
  }

  Widget _showErrorMessage() {
    if (_errorMessage.length > 0 && _errorMessage != null) {
      return Text(
        _errorMessage,
        style: TextStyle(
            fontSize: 13.0,
            color: Colors.red,
            height: 1.0,
            fontWeight: FontWeight.w300),
      );
    } else {
      return Container(
        height: 0.0,
      );
    }
  }

  Widget _showBody() {
    return Container(
      padding: EdgeInsets.all(16.0),
      margin: EdgeInsets.all(16.0),
      decoration: BoxDecoration(
        color: Color.fromRGBO(255, 255, 255, 0.5),
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
      ),
      child: Form(
        key: _formKey,
        child: ListView(
          shrinkWrap: true,
          children: <Widget>[
            _showEmailInput(),
            _showPasswordInput(),
            _showNameInput(),
            _showPrimaryButton(),
            _showSecondaryButton(),
            _showForgotButton(),
            _showErrorMessage(),
          ],
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kid_learning/common/custom_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:kid_learning/models/topic.dart';
import 'package:kid_learning/common/globals.dart' as globals;

class AchievementScreen extends StatefulWidget {
  AchievementScreen({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _AchievementScreenState();
}

class _AchievementScreenState extends State<AchievementScreen> {
  int starNum = 0;
  int easyHighScore = 0;
  int normalHighScore = 0;
  int hardHighScore = 0;

  List<dynamic> learnedLessons = [];
  @override
  void initState() {
    Firestore.instance
        .document('users/${globals.userId}')
        .get()
        .then((DocumentSnapshot ds) {
      setState(() {
        starNum = ds.data['learned_lessons'] == null
            ? 0
            : ds.data['learned_lessons'].length;
        learnedLessons = ds.data['learned_lessons'] == null
            ? []
            : ds.data['learned_lessons'];
        easyHighScore = ds.data['high_score'] == null
            ? 0
            : ds.data['high_score']['easy'] == null
                ? 0
                : ds.data['high_score']['easy'];
        normalHighScore = ds.data['high_score'] == null
            ? 0
            : ds.data['high_score']['normal'] == null
                ? 0
                : ds.data['high_score']['normal'];
        hardHighScore = ds.data['high_score'] == null
            ? 0
            : ds.data['high_score']['hard'] == null
                ? 0
                : ds.data['high_score']['hard'];
      });
    });
    super.initState();
  }

  Widget progressByLevel(String level) {
    return StreamBuilder<QuerySnapshot>(
      stream: Firestore.instance
          .collection('topics')
          .where('levels', arrayContains: level)
          .snapshots(),
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (!snapshot.hasData) return new Text('Loading...');
        return new Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: snapshot.data.documents.map((DocumentSnapshot document) {
            String textEn = document['title']['en'];
            String textVi = document['title']['vi'];
            String imageUrl = document['thumbnail'];
            return Padding(
              padding: const EdgeInsets.only(bottom: 30.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      FadeInImage.assetNetwork(
                        placeholder: 'images/loading.gif',
                        image: imageUrl,
                        width: 130.0,
                        height: 130.0,
                      ),
                      Text(textEn),
                      Text(textVi),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20.0),
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          level == 'easy'
                              ? Text('Mức độ dễ')
                              : level == 'normal'
                                  ? Text('Mức độ trung bình')
                                  : Text('Mức độ khó'),
                          StreamBuilder<QuerySnapshot>(
                            stream: Firestore.instance
                                .collection('lessons')
                                .where('level', isEqualTo: level)
                                .where('topic_ref',
                                    isEqualTo: document.reference)
                                .snapshots(),
                            builder: (BuildContext context,
                                AsyncSnapshot<QuerySnapshot> snapshot) {
                              if (!snapshot.hasData)
                                return new Text('Loading...');
                              return new Row(
                                children: snapshot.data.documents
                                    .map((DocumentSnapshot document) {
                                  return Padding(
                                    padding: const EdgeInsets.only(right: 10.0),
                                    child: Column(
                                      children: <Widget>[
                                        Text(
                                            'Bài ${document['no'].toString()}'),
                                        learnedLessons.contains(
                                                    document.documentID) ==
                                                true
                                            ? Image.asset('images/star.png')
                                            : Image.asset(
                                                'images/star-grey.png'),
                                      ],
                                    ),
                                  );
                                }).toList(),
                              );
                            },
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
            );
          }).toList(),
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        MainBackground(),
        Column(
          children: <Widget>[
            Center(
              child: Stack(
                alignment: AlignmentDirectional.center,
                children: <Widget>[
                  Image.asset('images/header-title.png'),
                  Column(
                    children: <Widget>[
                      Text(
                        'Thành tích',
                        style: TextStyle(fontSize: 30.0),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Expanded(
              child: Center(
                child: Container(
                  margin: EdgeInsets.all(20.0),
                  padding: EdgeInsets.all(20.0),
                  decoration: BoxDecoration(
                    color: Color.fromRGBO(255, 255, 255, 0.7),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: ListView(
                    scrollDirection: Axis.vertical,
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Text(
                            'Số sao đã đạt được: $starNum',
                            style: TextStyle(fontSize: 25.0),
                          ),
                          Image.asset(
                            'images/star-completed.gif',
                            width: 30.0,
                          )
                        ],
                      ),
                      Text(
                        'Điểm cao bài kiểm tra',
                        style: TextStyle(fontSize: 30.0),
                        textAlign: TextAlign.center,
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                            'Mức độ dễ: $easyHighScore',
                            style:
                                TextStyle(fontSize: 25.0, color: Colors.green),
                          ),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                            'Mức độ trung bình: $normalHighScore',
                            style:
                                TextStyle(fontSize: 25.0, color: Colors.orange),
                          ),
                        ],
                      ),
                      Row(
                        children: <Widget>[
                          Text(
                            'Mức độ khó: $hardHighScore',
                            style: TextStyle(fontSize: 25.0, color: Colors.red),
                          ),
                        ],
                      ),
                      Text(
                        'Tiến trình',
                        style: TextStyle(fontSize: 30.0),
                        textAlign: TextAlign.center,
                      ),
                      Text(
                        'Mức độ dễ',
                        style: TextStyle(fontSize: 27.0, color: Colors.green),
                      ),
                      progressByLevel('easy'),
                      Text(
                        'Mức độ trung bình',
                        style: TextStyle(fontSize: 27.0, color: Colors.orange),
                      ),
                      progressByLevel('normal'),
                      Text(
                        'Mức độ khó',
                        style: TextStyle(fontSize: 27.0, color: Colors.red),
                      ),
                      progressByLevel('hard'),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
        CommonButton(),
      ],
    ));
  }
}

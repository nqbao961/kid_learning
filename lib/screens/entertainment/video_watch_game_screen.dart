import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:kid_learning/common/globals.dart' as globals;
import 'package:kid_learning/screens/parent/slide_design_screen.dart';
import 'package:kid_learning/screens/parent/lesson_design_screen.dart';
import 'package:kid_learning/screens/parent/parent_control_screen.dart';
import 'package:kid_learning/common/custom_widget.dart';
import 'package:youtube_player/youtube_player.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class VideoWatchGameScreen extends StatefulWidget {
  final String videoUrl;

  VideoWatchGameScreen({Key key, @required this.videoUrl}) : super(key: key);

  @override
  State<StatefulWidget> createState() =>
      _VideoVideoWatchGameScreenState(videoUrl);
}

class _VideoVideoWatchGameScreenState extends State<VideoWatchGameScreen> {
  String videoUrl;
  _VideoVideoWatchGameScreenState(this.videoUrl);

  @override
  Widget build(BuildContext context) {
    globals.pauseBackgroundMusic();
    return WillPopScope(
      onWillPop: () {
        Navigator.pop(context, true);
        globals.resumeBackgroundMusic();
      },
      child: Scaffold(
        body: Stack(
          children: <Widget>[
            MainBackground(),
            Padding(
              padding: const EdgeInsets.fromLTRB(20, 50, 20, 20),
              child: YoutubePlayer(
                playerMode: YoutubePlayerMode.DEFAULT,
                context: context,
                source: videoUrl,
                quality: YoutubeQuality.MEDIUM,
              ),
            ),
            CommonButton()
          ],
        ),
      ),
    );
  }
}

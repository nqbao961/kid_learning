import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kid_learning/common/custom_widget.dart';
import 'package:kid_learning/common/globals.dart' as globals;
import 'package:kid_learning/screens/exam/level_examination.dart';
import 'package:kid_learning/screens/entertainment/video_selection_screen.dart';

class GameSelectionScreen extends StatelessWidget {

  GameSelectionScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          MainBackground(),
          CommonButton(),
          Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                  Padding(
                    padding: const EdgeInsets.fromLTRB(0,30,0,30),
                    child: GestureDetector(
                      onTap: () {
                        globals.playBtnSound(1);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => VideoSelectionScreen(),
                          ),
                        );
                      },
                      child: Stack(
                        alignment: AlignmentDirectional.center,
                        children: <Widget>[
                          Image.asset(
                            'images/button-blue.png',
                            width: 160.0,
                          ),
                          Text(
                            'Xem phim',
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white, fontSize: 16.0),
                          ),
                        ],
                      ),
                    ),
                  ),

                  GestureDetector(
                    onTap: () {
                      globals.playBtnSound(1);
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => VideoSelectionScreen(),
                        ),
                      );
                    },
                    child: Stack(
                      alignment: AlignmentDirectional.center,
                      children: <Widget>[
                        Image.asset(
                          'images/button-blue.png',
                          width: 160.0,
                        ),
                        Text(
                          'Chơi trò chơi',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white, fontSize: 16.0),
                        ),
                      ],
                    ),
                  ),



                ],
              )
          )
        ],
      ),
    );
  }
}
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:kid_learning/common/globals.dart' as globals;
import 'package:kid_learning/screens/parent/slide_design_screen.dart';
import 'package:kid_learning/screens/parent/lesson_design_screen.dart';
import 'package:kid_learning/screens/parent/parent_control_screen.dart';
import 'package:kid_learning/common/custom_widget.dart';
import 'package:kid_learning/screens/entertainment/video_watch_game_screen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import "dart:math";
import "package:kid_learning/models/game_video.dart";

class VideoSelectionScreen extends StatefulWidget {
  VideoSelectionScreen(
      {Key key})
      : super(key: key);


  @override
  State<StatefulWidget> createState() => _VideoSelectionScreenState();

}

class _VideoSelectionScreenState extends State<VideoSelectionScreen> {

  List<int> _colors = [
    0xFFf44336,
    0xFFe91e63,
    0xFF9c27b0,
    0xFF3f51b5,
    0xFF2196f3,
    0xFFffc107,
    0xFFff9800,
    0xFF8bc34a,
    0xFFff5722,
    0xFF673ab7,
    0xFF065535,
    0xFF0000FF,
    0xFFFFAACC,
    0xFF666666,
    0xFFEA3E3E,
    0xFF800000,
    0xFFBE79FD,
    0xFF00FF87,
    0xFF5B087F,
    0xFF33240C,
    0xFF02281C,
    0xFFD1784F,
    0xFF4F7AD1
  ];
//  List<String> titleList = [];

  @override
  Widget build(BuildContext context) {
    //globals.resumeBackgroundMusic();
    return Scaffold(
      body: Stack(
        children: <Widget>[
          MainBackground(),
          Align(
            alignment: Alignment.centerRight,
            child: Padding(
              padding: const EdgeInsets.only(top: 60),
              child: Container(
                width: 60,
                height: 100,
                child: Center(
                  child: Column(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 10, 0, 10),
                        child: Image.asset('images/arrow-up.png', width: 40,),
                      ),
                      Image.asset('images/arrow-down.png', width: 40,),
                    ],
                  ),
                ),
              ),
            ),
          ),
          StreamBuilder<QuerySnapshot>(
            stream:
            Firestore.instance.collection('video').snapshots(),
            builder: (BuildContext context,
                AsyncSnapshot<QuerySnapshot> snapshot) {
              if (!snapshot.hasData) return new Text('Loading...');
              return Padding(
                padding: const EdgeInsets.fromLTRB(10,50,50,0),
                child: GridView.count(
                    crossAxisCount: 3,
                    childAspectRatio: 7.0/9.0,
                    padding: const EdgeInsets.all(4.0),
                    mainAxisSpacing: 4.0,
                    crossAxisSpacing: 4.0,
                    children:
                      snapshot.data.documents
                          .map((DocumentSnapshot document) {
//                        titleList.add(document['title'].toString());
  //                  return LessonItem(widget.userId, document['no'].toString(), document.documentID);
//                      }).toList()

//                    <GameVideo>[
//                      GameVideo('https://media.giphy.com/media/3o7aCYSzqQufYAGEKc/source.gif', titleList[0]),
//                      GameVideo('https://media.giphy.com/media/3ohhwDEAzT48aVZRMA/source.gif', titleList[1]),
//                      GameVideo('https://media.giphy.com/media/3ohhwokifK1wY7eZ8s/source.gif', titleList[2]),
//                      GameVideo('https://media.giphy.com/media/l1J9Dk3bDyb5oooQE/source.gif', titleList[3]),
//                      GameVideo('https://media.giphy.com/media/3ohhwj5q17ISuJnBiU/source.gif', titleList[4]),
//                      GameVideo('https://media.giphy.com/media/ESo9piqgf8OVW/source.gif', titleList[5]),
//
//                    ]
//                        .map((GameVideo video) {
                      return new GridTile(
                          child: GestureDetector(
                            onTap: (){
                              globals.playBtnSound(2);
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => VideoWatchGameScreen(videoUrl: document['url'].toString()),
                                ),
                              );
                            },
                            child: Padding(
                                padding: const EdgeInsets.fromLTRB(10,10,10,10),
                                child: Container(
                                    color: Color(_colors[Random().nextInt(_colors.length)]),
                                    child: Column(
                                      children: <Widget>[
                                        Padding(
                                          padding: const EdgeInsets.fromLTRB(0,10,0,0),
                                          child: Text(
                                            document['title'].toString(),
                                            style: TextStyle(
                                                fontSize: 20.0,
                                                color: Colors.white
                                            ),
                                          ),
                                        ),

//                                        Image.network(document['gif'].toString(), fit: BoxFit.cover)
                                        Container(
                                          color: Colors.black54,
                                          width: 170,
                                          height: 200,
                                          child: Image.asset(document['gif'].toString(), fit: BoxFit.cover,),
                                        )

                                      ],
                                    )

                                )
                            ),
                          )
                      );
                    }).toList()),

              );
//                new ListView(
//                padding: EdgeInsets.only(top: 50.0),
//                scrollDirection: Axis.horizontal,
//                children:


//              );
            },
          ),


          CommonButton()
        ],
      ),
    );
  }
}
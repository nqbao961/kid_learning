import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kid_learning/common/custom_widget.dart';
import 'package:kid_learning/common/globals.dart' as globals;

import 'package:kid_learning/screens/entertainment/game_selection_screen.dart';


class ChooseLevelScreen extends StatelessWidget {
  final String userId;

  ChooseLevelScreen({Key key, @required this.userId}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        MainBackground(),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              height: 300.0,
              margin: EdgeInsets.only(top: 30.0, bottom: 30.0),
              child: Stack(
                children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      LevelButton(
                          'Dễ', 'images/button-easy.png', userId, 'easy'),
                      LevelButton('Trung bình', 'images/button-normal.png',
                          userId, 'normal'),
                      LevelButton(
                          'Nâng cao', 'images/button-hard.png', userId, 'hard'),
                      CustomLessonButton('Bài học tùy chỉnh',
                          'images/button-custom.png', userId),
                    ],
                  ),
                ],
              ),
            )
          ],
        ),
        CommonButton(),
      ],
    ));
  }
}

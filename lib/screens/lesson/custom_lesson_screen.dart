import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kid_learning/common/custom_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:kid_learning/models/topic.dart';
import 'package:kid_learning/screens/lesson/lesson_screen.dart';

class LessonItem extends StatelessWidget {
  final String userId;
  final String no;
  final String lessonId;
  LessonItem(this.userId, this.no, this.lessonId);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 50.0),
      child: GestureDetector(
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) =>
                  LessonScreen(userId: userId, lessonId: lessonId, lessonType: 'custom_lessons',),
            ),
          );
        },
        child: Column(
          children: <Widget>[
            Text('Bài $no', style: TextStyle(fontSize: 30.0),),
          ],
        ),
      ),
    );
  }
}

class CustomLessonScreen extends StatefulWidget {
  final String userId;

  CustomLessonScreen({Key key, @required this.userId})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _CustomLessonScreenState();
}

class _CustomLessonScreenState extends State<CustomLessonScreen> {
  //String _levelText;
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
          children: <Widget>[
            MainBackground(),
            Column(
              children: <Widget>[
                Center(
                  child: Stack(
                    alignment: AlignmentDirectional.center,
                    children: <Widget>[
                      Image.asset('images/header-title.png'),
                      Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(bottom: 10.0),
                            child: Text('Custom lesson', style: TextStyle(fontSize: 18.0),),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                Expanded(
                  child: Center(
                    child: Container(
                        margin: EdgeInsets.all(20.0),
                        padding: EdgeInsets.all(20.0),
                        decoration: BoxDecoration(
                          color: Color.fromRGBO(255, 255, 255, 0.5),
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: StreamBuilder<QuerySnapshot>(
                          stream:
                          Firestore.instance.collection('custom_lessons').snapshots(),
                          builder: (BuildContext context,
                              AsyncSnapshot<QuerySnapshot> snapshot) {
                            if (!snapshot.hasData) return new Text('Loading...');
                            return new ListView(
                              padding: EdgeInsets.only(top: 50.0),
                              scrollDirection: Axis.horizontal,
                              children: snapshot.data.documents
                                  .map((DocumentSnapshot document) {
                                return LessonItem(widget.userId, document['no'].toString(),
                                    document.documentID);
                              }).toList(),
                            );
                          },
                        )),
                  ),
                )
              ],
            ),
            CommonButton(),
          ],
        ));
  }
}

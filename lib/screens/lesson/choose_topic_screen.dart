import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kid_learning/common/custom_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:kid_learning/models/topic.dart';

class ChooseTopicScreen extends StatefulWidget {
  final String userId;
  final String level;

  ChooseTopicScreen({Key key, @required this.userId, @required this.level})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _ChooseTopicScreenState();
}

class _ChooseTopicScreenState extends State<ChooseTopicScreen> {
  String _levelText;
  @override
  void initState() {
    switch (widget.level) {
      case 'easy':
        _levelText = 'dễ';
        break;
      case 'normal':
        _levelText = 'trung bình';
        break;
      case 'hard':
        _levelText = 'nâng cao';
        break;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        MainBackground(),
        Column(
          children: <Widget>[
            Center(
              child: Stack(
                alignment: AlignmentDirectional.center,
                children: <Widget>[
                  Image.asset('images/header-title.png'),
                  Column(
                    children: <Widget>[
                      Text(
                        'Mức độ $_levelText',
                        style:
                        TextStyle(fontSize: 17.0, color: Colors.yellowAccent, shadows: [
                          Shadow(
                              offset: Offset(-1.5, -1.5),
                              color: Colors.black),
                          Shadow(
                              offset: Offset(1.5, -1.5),
                              color: Colors.black),
                          Shadow(
                              offset: Offset(1.5, 1.5),
                              color: Colors.black),
                          Shadow(
                              offset: Offset(-1.5, 1.5),
                              color: Colors.black),
                        ]),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 10.0),
                        child: Text(
                          'Chọn chủ đề',
                          style:
                          TextStyle(fontSize: 17.0, color: Colors.yellowAccent, shadows: [
                            Shadow(
                                offset: Offset(-1.5, -1.5),
                                color: Colors.black),
                            Shadow(
                                offset: Offset(1.5, -1.5),
                                color: Colors.black),
                            Shadow(
                                offset: Offset(1.5, 1.5),
                                color: Colors.black),
                            Shadow(
                                offset: Offset(-1.5, 1.5),
                                color: Colors.black),
                          ]),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Expanded(
              child: Center(
                child: Container(
                    margin: EdgeInsets.all(20.0),
                    padding: EdgeInsets.all(20.0),
                    decoration: BoxDecoration(
                      color: Color.fromRGBO(255, 255, 255, 0.5),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: StreamBuilder<QuerySnapshot>(
                      stream:
                          Firestore.instance.collection('topics').where('levels', arrayContains: widget.level).snapshots(),
                      builder: (BuildContext context,
                          AsyncSnapshot<QuerySnapshot> snapshot) {
                        if (!snapshot.hasData) return new Text('Loading...');
                        return new ListView(
                          scrollDirection: Axis.horizontal,
                          children: snapshot.data.documents
                              .map((DocumentSnapshot document) {
                            return TopicItem(widget.userId, document['title']['en'], document.data['title']['vi'],
                                document['thumbnail'], widget.level, document.reference, document.documentID);
                          }).toList(),
                        );
                      },
                    )),
              ),
            )
          ],
        ),
        CommonButton(),
      ],
    ));
  }
}

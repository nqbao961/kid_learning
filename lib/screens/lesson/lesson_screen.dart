import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:kid_learning/common/custom_widget.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:audioplayers/audio_cache.dart';
import "dart:math";
import 'package:kid_learning/common/globals.dart' as globals;
import 'package:cloud_functions/cloud_functions.dart';
import 'package:youtube_player/youtube_player.dart';
import 'package:auto_size_text/auto_size_text.dart';
import 'package:flutter/services.dart';
import 'package:animator/animator.dart';
import 'package:toast/toast.dart';

class LessonScreen extends StatefulWidget {
  final String userId;
  final String lessonId;
  final String lessonType;
  final String level;
  final String topicId;

  LessonScreen(
      {Key key,
      @required this.userId,
      @required this.lessonId,
      @required this.lessonType,
      this.level,
      this.topicId})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _LessonScreenState(lessonType);
}

class _LessonScreenState extends State<LessonScreen> {
  String lessonType;
  _LessonScreenState(this.lessonType);

  var mainSlider;
  List<Widget> _sliderItems = [];
  List<String> _correctStatement = [
    'Đúng rồi. Bé giỏi quá!',
    'Chính xác. Thật tuyệt vời!',
    'Chúc mừng bé. Đáp án chính xác!'
  ];
  List<int> _colors = [
    0xFFf44336,
    0xFFe91e63,
    0xFF9c27b0,
    0xFF3f51b5,
    0xFF2196f3,
    0xFFffc107,
    0xFFff9800,
    0xFF8bc34a,
    0xFFff5722,
    0xFF673ab7
  ];

  FlutterTts flutterTts;
  String language = 'en-US';

  AudioPlayer audioPlayer = AudioPlayer();
  AudioCache audioCache = new AudioCache();

  List quizList;

  final _formKey = GlobalKey<FormState>();
  var _answerInput = new TextEditingController();

  bool _canScroll = true;
  int _current = 0;
  int _slideCount = 0;
  int _quizCount = 0;

  Stream<DocumentSnapshot> _lessonStream;
  //Future<DocumentSnapshot> _quizFuture;
  final AsyncMemoizer _memoizer = AsyncMemoizer();

  @override
  void initState() {
    removeManual();

    initTts();
    super.initState();
  }

  @override
  void dispose() {
    _answerInput.dispose();
    super.dispose();
  }

  initTts() {
    flutterTts = FlutterTts();
    flutterTts.setLanguage(language);
    flutterTts.setVoice(globals.voiceType);
    flutterTts.setVolume(globals.volume);
  }

  void genSlideList(slide) {
    slide.snapshots().listen((data) {
      _slideCount++;
      String textEn = data['content']['en'];
      String pronounce = data['content']['pronounce'];
      String textVi = data['content']['vi'];
      String imageUrl = data['image'];
      String videoUrl = data['video'];
      String gifUrl = data['gif'];
      _sliderItems.add(Builder(
        builder: (BuildContext context) {
          flutterTts.speak(textEn);
          return GestureDetector(
            onTap: () {
              flutterTts.speak(textEn);
            },
            child: Container(
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(color: Colors.white),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      flex: 2,
                      child: AutoSizeText(
                        textEn,
                        style: TextStyle(
                            fontSize: 110.0,
                            color: Color(
                                _colors[Random().nextInt(_colors.length)])),
                        group: AutoSizeGroup(),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: AutoSizeText(
                        pronounce,
                        style: TextStyle(fontSize: 35.0, fontFamily: "Roboto"),
                        group: AutoSizeGroup(),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: AutoSizeText(
                        textVi,
                        style: TextStyle(fontSize: 35.0),
                        group: AutoSizeGroup(),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          );
        },
      ));
      _sliderItems.add(Builder(
        builder: (BuildContext context) {
          flutterTts.speak(textEn);
          return GestureDetector(
            onTap: () {
              flutterTts.speak(textEn);
            },
            child: Container(
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(color: Colors.white),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      flex: 3,
                      child: Container(
                        width: 500.0,
                        height: 270.0,
                        child: FadeInImage.assetNetwork(
                          placeholder: 'images/loading2.gif',
                          image: imageUrl,
                          fit: BoxFit.contain,
                        ),
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: AutoSizeText(
                        textEn,
                        style: TextStyle(
                            fontSize: 40.0,
                            color: Color(
                                _colors[Random().nextInt(_colors.length)])),
                        group: AutoSizeGroup(),
                      ),
                    )
                  ],
                ),
              ),
            ),
          );
        },
      ));
      if (gifUrl != '') {
        _sliderItems.add(Builder(
          builder: (BuildContext context) {
            flutterTts.speak(textEn);
            return GestureDetector(
              onTap: () {
                flutterTts.speak(textEn);
              },
              child: Container(
                width: MediaQuery.of(context).size.width,
                decoration: BoxDecoration(color: Colors.white),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: 500.0,
                      height: 300.0,
                      child: FadeInImage.assetNetwork(
                        placeholder: 'images/loading2.gif',
                        image: gifUrl,
                        fit: BoxFit.contain,
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ));
      }
      if (videoUrl != '') {
        _sliderItems.add(YoutubePlayer(
          context: context,
          source: videoUrl,
          loop: true,
          quality: YoutubeQuality.MEDIUM,
          playerMode: YoutubePlayerMode.DEFAULT,
        ));
      }
    });
  }

  void genQuizList(quiz) {
    _quizCount++;
    _sliderItems.add(StreamBuilder<DocumentSnapshot>(
      stream: quiz.snapshots(),
      builder:
          (BuildContext context, AsyncSnapshot<DocumentSnapshot> snapshot) {
        print(snapshot.hasData);
        if (!snapshot.hasData) return new Text('Loading...');
        String questionContent = snapshot.data['question']['content'];
        String questionImage = snapshot.data['question']['image'];
        String questionPronounce = snapshot.data['question']['pronounce'];
        String choiceA = snapshot.data['choices'][0];
        String choiceB = snapshot.data['choices'][1];
        String choiceC = snapshot.data['choices'][2];
        String choiceD = snapshot.data['choices'][3];
        String answer = snapshot.data['answer_fill_blank'] == ""
            ? snapshot.data['choices'][snapshot.data['answer_multiple_choice']]
            : snapshot.data['answer_fill_blank'];
        String type = snapshot.data['type'];
        String quizId = snapshot.data.documentID;
        Firestore.instance
            .document('quizzes/$quizId')
            .get()
            .then((DocumentSnapshot ds) {
          if (!ds.data['results'].containsKey(widget.userId)) {
            Firestore.instance.document('quizzes/$quizId').setData({
              'results': {
                widget.userId: {'correct_num': 0, 'incorrect_num': 0}
              }
            }, merge: true);
          }
        });
        if (type == 'multiple-choice') {
          flutterTts.speak(questionPronounce);
          return Container(
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(color: Colors.white),
              child: Column(
                children: <Widget>[
                  Expanded(
                    flex: 2,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        questionContent != ''
                            ? Expanded(
                                flex: 1,
                                child: Center(
                                  child: Text(
                                    questionContent,
                                    style: TextStyle(fontSize: 20.0),
                                  ),
                                ),
                              )
                            : Container(),
                        questionImage != ''
                            ? Expanded(
                                flex: 2,
                                child: GestureDetector(
                                  onTap: () {
                                    flutterTts.speak(questionPronounce);
                                  },
                                  child: FadeInImage.assetNetwork(
                                    placeholder: 'images/loading2.gif',
                                    image: questionImage,
                                    fit: BoxFit.contain,
                                  ),
                                ))
                            : questionPronounce != ''
                                ? GestureDetector(
                                    onTap: () {
                                      flutterTts.speak(questionPronounce);
                                    },
                                    child: Icon(
                                      Icons.volume_up,
                                      size: 100.0,
                                    ),
                                  )
                                : Container(),
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Column(
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            choiceSelector('A.', choiceA, answer, quizId),
                            choiceSelector('B.', choiceB, answer, quizId)
                          ],
                        )
                      ],
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: Column(
                      children: <Widget>[
                        Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: <Widget>[
                            choiceSelector('C.', choiceC, answer, quizId),
                            choiceSelector('D.', choiceD, answer, quizId)
                          ],
                        )
                      ],
                    ),
                  )
                ],
              ));
        } else if (type == 'fill-blank') {
          flutterTts.speak(questionPronounce);
          return Container(
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(color: Colors.white),
              child: ListView(
                shrinkWrap: true,
                children: <Widget>[
                  Container(
                    height: MediaQuery.of(context).size.height / 5.0,
                    child: Center(
                      child: Text(
                        questionContent,
                        style: TextStyle(fontSize: 20.0),
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height / 2.0,
                    child: GestureDetector(
                      onTap: () {
                        flutterTts.speak(questionPronounce);
                      },
                      child: questionImage != ''
                          ? FadeInImage.assetNetwork(
                              placeholder: 'images/loading2.gif',
                              image: questionImage)
                          : Icon(
                              Icons.volume_up,
                              size: 200.0,
                            ),
                    ),
                  ),
                  Container(
                    child: Form(
                      key: _formKey,
                      child: Column(
                        children: <Widget>[
                          Container(
                            width: 200.0,
                            child: TextFormField(
                              controller: _answerInput,
                              maxLines: 1,
                              textAlign: TextAlign.center,
                              keyboardType: TextInputType.text,
                              autofocus: false,
                              onSaved: (value) =>
                                  _answerInput.text = value.toLowerCase(),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(top: 8.0),
                            child: GestureDetector(
                              onTap: () {
                                globals.playBtnSound(1);
                                SystemChrome.setEnabledSystemUIOverlays([]);
                                FocusScope.of(context)
                                    .requestFocus(new FocusNode());
                                final form = _formKey.currentState;
                                form.save();
                                checkAnswerInput(answer, quizId);
                              },
                              child: Stack(
                                alignment: AlignmentDirectional.center,
                                children: <Widget>[
                                  Image.asset(
                                    'images/button-blue.png',
                                    width: 160.0,
                                  ),
                                  Text(
                                    'Check',
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 16.0),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ],
              ));
        }
      },
    ));
  }

  void genCompletedSlide() {
    _sliderItems.add(Builder(
      builder: (BuildContext context) {
        audioCache.play('sounds/success_sound.wav', volume: globals.volume);
        audioCache.play('sounds/kids_saying_yay.mp3', volume: globals.volume);
        return GestureDetector(
          onTap: () {
            Firestore.instance.document('users/${widget.userId}').setData({
              'learned_lessons': FieldValue.arrayUnion([widget.lessonId])
            }, merge: true);
            Navigator.of(context).pop();
            globals.resumeBackgroundMusic();
          },
          child: Container(
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(color: Colors.white),
            child: Stack(
              children: <Widget>[
                Container(
                  width: MediaQuery.of(context).size.width,
                  child: Image.asset(
                    'images/star-background.gif',
                    repeat: ImageRepeat.repeatX,
                  ),
                ),
                Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        width: 500.0,
                        height: 250.0,
                        child: Image.asset(
                          'images/star-completed.gif',
                          fit: BoxFit.contain,
                        ),
                      ),
                      Text(
                        'Chúc mừng bé đã đạt được 1 sao ^^!',
                        style: TextStyle(
                            fontSize: 30.0,
                            color: Color(
                                _colors[Random().nextInt(_colors.length)])),
                      ),
                      Animator(
                        repeats: 0,
                        builder: (Animation anim) => Opacity(
                              opacity: anim.value,
                              child: Text('Chạm vào để nhận sao nhé!'),
                            ),
                      )
                    ],
                  ),
                )
              ],
            ),
          ),
        );
      },
    ));
  }

  void checkAnswer(choice, answer, quizId) {
    final DocumentReference postRef =
        Firestore.instance.document('quizzes/$quizId');
    Firestore.instance.runTransaction((Transaction tx) async {
      DocumentSnapshot postSnapshot = await tx.get(postRef);
      if (postSnapshot.exists) {
        var curCorrectNum =
            postSnapshot.data['results'][widget.userId]['correct_num'];
        var curIncorrectNum =
            postSnapshot.data['results'][widget.userId]['incorrect_num'];
        var curTotalCorrectNum =
            postSnapshot.data['total_result']['correct_num'];
        var curTotalIncorrectNum =
            postSnapshot.data['total_result']['incorrect_num'];
        Map allResults = postSnapshot.data['results'];
        Map userResults = choice == answer
            ? {
                'correct_num': curCorrectNum == null ? 0 : curCorrectNum + 1,
                'incorrect_num': curIncorrectNum == null ? 0 : curIncorrectNum
              }
            : {
                'correct_num': curCorrectNum == null ? 0 : curCorrectNum,
                'incorrect_num':
                    curIncorrectNum == null ? 0 : curIncorrectNum + 1
              };
        Map newTotalResult = choice == answer
            ? {
                'correct_num': curTotalCorrectNum + 1,
                'incorrect_num': curTotalIncorrectNum
              }
            : {
                'correct_num': curTotalCorrectNum,
                'incorrect_num': curTotalIncorrectNum + 1
              };
        allResults.update(widget.userId, (val) => userResults);
        await tx
            .update(postRef, <String, dynamic>{'total_result': newTotalResult});
        await tx.update(postRef, <String, dynamic>{'results': allResults});
      }
    });
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        Future.delayed(Duration(milliseconds: 2500), () {
          Navigator.of(context).pop(true);
          if (choice == answer)
            mainSlider.nextPage(
                duration: Duration(milliseconds: 300), curve: Curves.linear);
        });

        choice == answer
            ? audioCache.play('sounds/correct.mp3', volume: globals.volume)
            : audioCache.play('sounds/incorrect.mp3', volume: globals.volume);
        return AlertDialog(
          content: choice == answer
              ? Text(
                  _correctStatement[Random().nextInt(_correctStatement.length)],
                  style: TextStyle(color: Colors.green, fontSize: 22.0))
              : Text('Sai mất rồi. Đáp án đúng là "$answer".',
                  style: TextStyle(color: Colors.red, fontSize: 22.0)),
        );
      },
    );
  }

  void checkAnswerInput(answer, quizId) {
    final DocumentReference postRef =
        Firestore.instance.document('quizzes/$quizId');
    Firestore.instance.runTransaction((Transaction tx) async {
      DocumentSnapshot postSnapshot = await tx.get(postRef);
      if (postSnapshot.exists) {
        var curCorrectNum =
            postSnapshot.data['results'][globals.userId]['correct_num'];
        var curIncorrectNum =
            postSnapshot.data['results'][globals.userId]['incorrect_num'];
        var curTotalCorrectNum =
            postSnapshot.data['total_result']['correct_num'];
        var curTotalIncorrectNum =
            postSnapshot.data['total_result']['incorrect_num'];
        Map allResults = postSnapshot.data['results'];
        Map userResults = _answerInput.text == answer
            ? {
                'correct_num': curCorrectNum == null ? 0 : curCorrectNum + 1,
                'incorrect_num': curIncorrectNum == null ? 0 : curIncorrectNum
              }
            : {
                'correct_num': curCorrectNum == null ? 0 : curCorrectNum,
                'incorrect_num':
                    curIncorrectNum == null ? 0 : curIncorrectNum + 1
              };
        Map newTotalResult = _answerInput.text == answer
            ? {
                'correct_num': curTotalCorrectNum + 1,
                'incorrect_num': curTotalIncorrectNum
              }
            : {
                'correct_num': curTotalCorrectNum,
                'incorrect_num': curTotalIncorrectNum + 1
              };
        allResults.update(globals.userId, (val) => userResults);
        await tx
            .update(postRef, <String, dynamic>{'total_result': newTotalResult});
        await tx.update(postRef, <String, dynamic>{'results': allResults});
      }
    });
    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        _answerInput.text == answer
            ? audioCache.play('sounds/correct.mp3', volume: globals.volume)
            : audioCache.play('sounds/incorrect.mp3', volume: globals.volume);
        return AlertDialog(
          content: _answerInput.text == answer
              ? Text(
                  _correctStatement[Random().nextInt(_correctStatement.length)],
                  style: TextStyle(color: Colors.green, fontSize: 22.0))
              : Text('Sai mất rồi. Đáp án đúng là "$answer".',
                  style: TextStyle(color: Colors.red, fontSize: 22.0)),
        );
      },
    );
    Future.delayed(Duration(milliseconds: 2500), () {
      Navigator.of(context).pop(true);
      if (_answerInput.text == answer)
        mainSlider.nextPage(
            duration: Duration(milliseconds: 300), curve: Curves.linear);
    });
  }

  Widget choiceSelector(index, choice, answer, quizId) {
    return GestureDetector(
      onTap: () {
        globals.playBtnSound(2);
        checkAnswer(choice, answer, quizId);
      },
      child: Stack(
        alignment: AlignmentDirectional.center,
        children: <Widget>[
          Image.asset(
            'images/button-blue.png',
            width: 160.0,
          ),
          Text(
            '$index $choice',
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white, fontSize: 16.0),
          ),
        ],
      ),
    );
  }

  Future<DocumentSnapshot> getQuizList() async {
    try {
      await CloudFunctions.instance.call(
        functionName: 'getQuizList',
        parameters: <String, dynamic>{
          'level': widget.level,
          'topic': widget.topicId,
          'userId': widget.userId
        },
      );
    } on CloudFunctionsException catch (e) {
      print('caught firebase functions exception');
      print(e.code);
      print(e.message);
      print(e.details);
    } catch (e) {
      print('caught generic exception');
      print(e);
    }
    return Firestore.instance
        .collection('users')
        .document(globals.userId)
        .get();
  }

  _quizFuture() {
    return _memoizer.runOnce(() async {
      try {
        await CloudFunctions.instance.call(
          functionName: 'getQuizList',
          parameters: <String, dynamic>{
            'level': widget.level,
            'topic': widget.topicId,
            'userId': widget.userId
          },
        );
      } on CloudFunctionsException catch (e) {
        print('caught firebase functions exception');
        print(e.code);
        print(e.message);
        print(e.details);
      } catch (e) {
        print('caught generic exception');
        print(e);
      }
      return Firestore.instance
          .collection('users')
          .document(globals.userId)
          .get();
    });
  }

  Future<bool> _onBackPress() {
    return showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: Text('Bạn có muốn thoát bài học không?'),
              actions: <Widget>[
                FlatButton(
                  child: Text(
                    'Có',
                    style: TextStyle(color: Colors.red, fontSize: 20.0),
                  ),
                  onPressed: () {
                    Navigator.pop(context, true);
                    Navigator.pop(context, true);
                    globals.resumeBackgroundMusic();
                  },
                ),
                FlatButton(
                  child: Text(
                    'Không',
                    style: TextStyle(color: Colors.green, fontSize: 20.0),
                  ),
                  onPressed: () {
                    Navigator.pop(context, true);
                  },
                ),
              ],
            ));
  }

  List<T> map<T>(List list, Function handler) {
    List<T> result = [];
    for (var i = 0; i < list.length; i++) {
      result.add(handler(i, list[i]));
    }

    return result;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () {
        _onBackPress();
      },
      child: Scaffold(
        body: Stack(
          children: <Widget>[
            MainBackground(),
            StreamBuilder(
              stream: _lessonStream,
              builder: (BuildContext context, lessonSnapshot) {
                globals.pauseBackgroundMusic();
                if (!lessonSnapshot.hasData)
                  return Center(
                    child: Image.asset('images/loading2.gif', width: 100.0),
                  );
                List slideReference = lessonSnapshot.data['slides'];
                //getQuizList();
                if (_slideCount != slideReference.length) {
                  slideReference.forEach((slide) => genSlideList(slide));
                }

                return FutureBuilder(
                  future: _quizFuture(),
                  builder: (BuildContext context, testSnapshot) {
                    if (!testSnapshot.hasData)
                      return Center(
                        child: Image.asset('images/loading2.gif', width: 100.0),
                      );
                    List quizReference = testSnapshot.data['quizList'];
                    if (_quizCount != quizReference.length) {
                      quizReference.forEach((quiz) => genQuizList(quiz));
                      genCompletedSlide();
                    }
                    mainSlider = CarouselSlider(
                      enableInfiniteScroll: false,
                      enableScroll: _canScroll,
                      viewportFraction: 1.1,
                      autoPlay: false,
                      height: 400.0,
                      items: _sliderItems,
                      onPageChanged: (index) {
                        _answerInput.clear();
                        setState(() {
                          _current = index;
                          if (index + 1 == _sliderItems.length) {
                            _canScroll = false;
                          }
                        });
                      },
                    );
                    return Stack(
                      children: <Widget>[
                        mainSlider,
                        Container(
                          alignment: Alignment.bottomCenter,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: map<Widget>(
                              _sliderItems,
                              (index, url) {
                                return Container(
                                  width: 8.0,
                                  height: 8.0,
                                  margin: EdgeInsets.symmetric(
                                      vertical: 10.0, horizontal: 2.0),
                                  decoration: BoxDecoration(
                                      shape: BoxShape.circle,
                                      color: _current == index
                                          ? Color.fromRGBO(0, 0, 0, 0.9)
                                          : Color.fromRGBO(0, 0, 0, 0.4)),
                                );
                              },
                            ),
                          ),
                        ),
                      ],
                    );
                  },
                );
              },
            ),
            Container(
              alignment: Alignment.topRight,
              child: GestureDetector(
                onTap: () {
                  _onBackPress();
                },
                child: Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Image.asset('images/uncheck-icon.png', width: 35.0),
                ),
              ),
            ),
            manual
          ],
        ),
      ),
    );
  }

  void removeManual() {
    Future.delayed(Duration(milliseconds: 5000), () {
      setState(() {
        manual = Container();
        _lessonStream = Firestore.instance
            .collection(lessonType)
            .document(widget.lessonId)
            .snapshots();
        //_quizFuture = getQuizList();
      });
    });
  }

  Widget manual = Container(
    decoration: BoxDecoration(
      color: Color.fromRGBO(0, 0, 0, 0.8),
    ),
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Image.asset('images/swipe-right.png'),
            Image.asset('images/single-touch.png'),
            Image.asset('images/swipe-left.png'),
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Container(
                width: 150.0,
                child: Text(
                  'Vuốt sang phải để trở về slide trước',
                  style: TextStyle(fontSize: 18.0, color: Colors.white),
                  textAlign: TextAlign.center,
                )),
            Container(
                width: 150.0,
                child: Text(
                  'Chạm vào để nghe phát âm',
                  style: TextStyle(fontSize: 18.0, color: Colors.white),
                  textAlign: TextAlign.center,
                )),
            Container(
                width: 150.0,
                child: Text(
                  'Vuốt sang trái để qua slide tiếp theo',
                  style: TextStyle(fontSize: 18.0, color: Colors.white),
                  textAlign: TextAlign.center,
                )),
          ],
        ),
      ],
    ),
  );
}

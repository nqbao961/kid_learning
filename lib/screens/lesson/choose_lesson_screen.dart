import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kid_learning/common/custom_widget.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:kid_learning/models/topic.dart';
import 'package:kid_learning/screens/lesson/lesson_screen.dart';
import 'package:kid_learning/common/globals.dart' as globals;

class LessonItem extends StatelessWidget {
  final String userId;
  final int no;
  final String lessonId;
  final String level;
  final String topicId;
  LessonItem(this.userId, this.no, this.lessonId, this.level, this.topicId);

  List<String> lessonIcons = [
    'images/lesson-1.png',
    'images/lesson-2.png',
    'images/lesson-3.png',
    'images/lesson-4.png',
    'images/lesson-5.png',
    'images/lesson-6.png',
    'images/lesson-7.png',
    'images/lesson-8.png',
    'images/lesson-9.png',
    'images/lesson-10.png',
    'images/lesson-11.png',
    'images/lesson-12.png',
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 50.0),
      child: GestureDetector(
        onTap: () {
          globals.playBtnSound(1);
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) =>
                  LessonScreen(userId: userId, lessonId: lessonId, lessonType: 'lessons', level: level, topicId: topicId,),
            ),
          );
        },
        child: Column(
          children: <Widget>[
//            Text('Bài $no', style: TextStyle(fontSize: 30.0),),
            Container(
              width: 130,
              height: 130,
              child: Stack(
                children: <Widget>[
//                  Center(
//                    child: Image.asset('images/wooden-frame.png', fit: BoxFit.fill,),
//                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 35),
                    child: Center(
                      child: Container(
                        width: 150,
                        height: 100,
                        child: Image.asset(lessonIcons[no-1], fit: BoxFit.fill,),
                      )
                    ),
                  ),
//                  Center(
//                    child: Image.asset('images/star-learned.gif', fit: BoxFit.fill),
//                  )
                ],
              )
            )
          ],
        ),
      ),
    );
  }
}

class ChooseLessonScreen extends StatefulWidget {
  final String userId;
  final String level;
  final String textEn;
  final DocumentReference topic;
  final String topicId;

  ChooseLessonScreen({Key key, @required this.userId, @required this.level, @required this.textEn, @required this.topic, @required this.topicId})
      : super(key: key);

  @override
  State<StatefulWidget> createState() => _ChooseLessonScreenState();
}

class _ChooseLessonScreenState extends State<ChooseLessonScreen> {
  String _levelText;

  @override
  void initState() {
    switch (widget.level) {
      case 'easy':
        _levelText = 'dễ';
        break;
      case 'normal':
        _levelText = 'trung bình';
        break;
      case 'hard':
        _levelText = 'nâng cao';
        break;
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    return Scaffold(
        body: Stack(
      children: <Widget>[
        MainBackground(),
        Column(
          children: <Widget>[
            Center(
              child: Stack(
                alignment: AlignmentDirectional.center,
                children: <Widget>[
                  Image.asset('images/header-title.png'),
                  Column(
                    children: <Widget>[
                      Text(
                        'Mức độ $_levelText',
                        style:
                        TextStyle(fontSize: 17.0, color: Colors.yellowAccent, shadows: [
                          Shadow(
                              offset: Offset(-1.5, -1.5),
                              color: Colors.black),
                          Shadow(
                              offset: Offset(1.5, -1.5),
                              color: Colors.black),
                          Shadow(
                              offset: Offset(1.5, 1.5),
                              color: Colors.black),
                          Shadow(
                              offset: Offset(-1.5, 1.5),
                              color: Colors.black),
                        ]),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(bottom: 10.0),
                        child: Text(
                          widget.textEn,
                          style:
                          TextStyle(fontSize: 17.0, color: Colors.yellowAccent, shadows: [
                            Shadow(
                                offset: Offset(-1.5, -1.5),
                                color: Colors.black),
                            Shadow(
                                offset: Offset(1.5, -1.5),
                                color: Colors.black),
                            Shadow(
                                offset: Offset(1.5, 1.5),
                                color: Colors.black),
                            Shadow(
                                offset: Offset(-1.5, 1.5),
                                color: Colors.black),
                          ]),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
            Expanded(
              child: Center(
                child: Container(
                    margin: EdgeInsets.all(20.0),
                    padding: EdgeInsets.all(20.0),
//                    decoration: BoxDecoration(
//                      color: Color.fromRGBO(255, 255, 255, 0.5),
//                      borderRadius: BorderRadius.circular(10),
//                    ),
                    child: StreamBuilder<QuerySnapshot>(
                      stream:
                          Firestore.instance.collection('lessons').where('level', isEqualTo: widget.level).where('topic_ref', isEqualTo: widget.topic).snapshots(),
                      builder: (BuildContext context,
                          AsyncSnapshot<QuerySnapshot> snapshot) {
                        if (!snapshot.hasData) return new Text('Loading...');
                        return new ListView(
                          padding: EdgeInsets.only(top: 50.0),
                          scrollDirection: Axis.horizontal,
                          children: snapshot.data.documents
                              .map((DocumentSnapshot document) {
                            return LessonItem(widget.userId, document['no'],
                                document.documentID, widget.level, widget.topicId);
                          }).toList(),
                        );
                      },
                    )),
              ),
            )
          ],
        ),
        CommonButton(),
      ],
    ));
  }
}

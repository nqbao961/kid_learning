import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kid_learning/common/custom_widget.dart';
import 'package:kid_learning/common/globals.dart' as globals;
import 'package:kid_learning/models/topic.dart';
import 'package:kid_learning/screens/parent/slide_design_screen.dart';
import 'package:kid_learning/screens/parent/lesson_design_screen.dart';

class LessonListScreen extends StatefulWidget {
  LessonListScreen({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _LessonListScreenState();
}

class _LessonListScreenState extends State<LessonListScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      children: <Widget>[
        MainBackground(),
        Padding(
          padding: const EdgeInsets.fromLTRB(10, 55, 15, 10),
          child: Center(
            child: Row(
              children: <Widget>[
                Center(
                  child: Container(
                    height: 100,
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.fromLTRB(10, 0, 10, 10),
                          child: Image.asset('images/arrow-up.png', width: 40,),
                        ),
                        Image.asset('images/arrow-down.png', width: 40,),
                      ],
                    ),
                  ),
                ),
                Container(
                    width: 630,
                    padding: EdgeInsets.all(10.0),
                    decoration: BoxDecoration(
                      image: DecorationImage(
                        image: AssetImage('images/wooden-background.jpg'),
                        fit: BoxFit.fill,
                      ),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: StreamBuilder<QuerySnapshot>(
                      stream: Firestore.instance.collection('slides').snapshots(),
                      builder: (BuildContext context,
                          AsyncSnapshot<QuerySnapshot> snapshot) {
                        if (!snapshot.hasData) return new Text('Loading...');
                        return new ListView(
                          scrollDirection: Axis.vertical,
                          children: snapshot.data.documents
                              .map<Widget>((DocumentSnapshot document) {
                            return _slideListItem(document);
                          }).toList(),
                        );
                      },
                    )),
              ],
            ),
          ),
        ),
        hoverBtn(),
        CommonButton()
      ],
    ));
  }

  Align hoverBtn() {
    return Align(
        alignment: FractionalOffset.bottomLeft,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(10, 0, 0, 10),
          child: GestureDetector(
            onTap: (){
              globals.playBtnSound(1);
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => LessonDesignScreen(),
                ),
              );
            },
            child: Stack(
              alignment: AlignmentDirectional.center,
              children: <Widget>[
                Image.asset('images/add-icon.png', width: 60,),
              ],
            ),
          ),
        ),
      );
  }

  @override
  void initState() {
    super.initState();
  }

  _areYouSureDialog(BuildContext context, String slideTitle) {
    return SimpleDialog(
      title: Text('Bạn có muốn xóa slide ${slideTitle} không?'),
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            GestureDetector(
                onTap: () {},
                child: Stack(
                  alignment: AlignmentDirectional.center,
                  children: <Widget>[
                    Image.asset('images/button-blue.png', width: 160.0),
                    Text('Có',
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white, fontSize: 16.0))
                  ],
                )),
            GestureDetector(
                onTap: () {},
                child: Stack(
                  alignment: AlignmentDirectional.center,
                  children: <Widget>[
                    Image.asset('images/button-red.png', width: 160.0),
                    Text('Không',
                        textAlign: TextAlign.center,
                        style: TextStyle(color: Colors.white, fontSize: 16.0))
                  ],
                ))
          ],
        )
      ],
    );
  }

  _slideListItem(DocumentSnapshot d) {
    return Padding(
      padding: const EdgeInsets.fromLTRB(0, 5.0, 0, 5.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Container(
              decoration: BoxDecoration(
                color: Color.fromRGBO(256, 256, 256, 0.7),
                borderRadius: BorderRadius.circular(10.0),
              ),
              child: Padding(
                padding: const EdgeInsets.fromLTRB(20, 10, 0, 10),
                child: Text(
                  d['content']['vi'],
                  overflow: TextOverflow.ellipsis,
                  style: TextStyle(fontSize: 25.0, color: Colors.white),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 0, 0, 0),
            child: GestureDetector(
              onTap: () {
                showDialog(
                    context: context,
                    builder: (context) =>
                        _areYouSureDialog(context, d['content']['vi']));
              },
              child: Stack(
                alignment: AlignmentDirectional.center,
                children: <Widget>[
                  Image.asset(
                    'images/delete-icon.png',
                    width: 50,
                  )
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(10, 0, 10, 0),
            child: GestureDetector(
              onTap: () {
              },
              child: Stack(
                alignment: AlignmentDirectional.center,
                children: <Widget>[
                  Image.asset(
                    'images/edit-icon.png',
                    width: 40,
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}

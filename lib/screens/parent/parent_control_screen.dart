import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:kid_learning/common/custom_widget.dart';

class ParentControlScreen extends StatefulWidget {
  ParentControlScreen(
      {Key key})
      : super(key: key);


  @override
  State<StatefulWidget> createState() => _ParentControlScreenState();

}

class _ParentControlScreenState extends State<ParentControlScreen> {


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.transparent,
      body: Stack(
        children: <Widget>[
          MainBackground(),
          CommonButton(),
          Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

                ],
              )
          )
        ],
      ),
    );
  }
}
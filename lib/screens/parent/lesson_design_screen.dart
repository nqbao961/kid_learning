import 'package:flutter/material.dart';
import 'package:kid_learning/common/custom_widget.dart';
import 'package:kid_learning/screens/parent/parent_selection_screen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'dart:async';
import 'package:toast/toast.dart';
import 'package:kid_learning/common/globals.dart' as globals;

class LessonDesignScreen extends StatefulWidget {
  LessonDesignScreen({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _LessonDesignScreenState();
}

class _LessonDesignScreenState extends State<LessonDesignScreen> {
  static String defaultValue = 'Chữ A, a';
  static int defaultId = 0;

  List<String> _quiz = [defaultValue];
  List documentId = [defaultId];

  List<DocumentSnapshot> _questionList;
  int lessonNo = 1;

  Row generateSlideRow(
      String rowTitle, List<DocumentSnapshot> inputList, int position) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Text(rowTitle,
            textAlign: TextAlign.center,
            style: TextStyle(
                color: Colors.black,
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
                fontFamily: 'Pony')),
        DropdownButton(
          hint: new Text("Hãy chọn slide"),
          value: _quiz[position],
          onChanged: (newValue) {
            setState(() {
              _quiz[position] = newValue;
            });
          },
          style: TextStyle(
              color: Colors.black,
              fontSize: 20.0,
              fontWeight: FontWeight.bold,
              fontFamily: 'Pony'),
          items: inputList
              .map((question) => DropdownMenuItem(
                  value: question['content']['vi'],
                  child: Container(
                    width: MediaQuery.of(context).size.width * 0.7,
                    color: Colors.white,
                    child: Text(
                      question['content']['vi'],
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  )))
              .toList(),
        )
      ],
    );
  }

  createTest() async {
    globals.playBtnSound(1);

    for (var i = 0; i < _quiz.length; i++) {
      for (var j = 0; j < _questionList.length; j++) {
        if (_questionList[j]['content']['vi'] == _quiz[i]) {
          documentId[i] = _questionList[j].documentID;
        }
      }
    }

    List slideLinks = documentId
        .map((slide) => Firestore.instance.collection('slides').document(slide))
        .toList();
    Firestore.instance
        .collection('custom_lessons')
        .add({'slides': slideLinks, 'no': lessonNo});

    toastConfirm();
  }

  void toastConfirm() async {
//    Fluttertoast.showToast(
//        msg: "Bài học mới đã được thêm",
//        toastLength: Toast.LENGTH_SHORT,
//        gravity: ToastGravity.CENTER,
//        timeInSecForIos: 1,
//        backgroundColor: Colors.brown,
//        textColor: Colors.white,
//        fontSize: 16.0
//    );
    Toast.show("Bài học mới đã được thêm thành công", context,
        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
    Future.delayed(const Duration(milliseconds: 200), () {
      setState(() {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ParentSelectionScreen(),
          ),
        );
      });
    });
  }

  void addSlide() {
    setState(() {
      _quiz.add(defaultValue);
      documentId.add(defaultId);
    });
  }

  Widget generateListItem(BuildContext context, int index) {
    return Card(
      child: generateSlideRow("Slide số ${index + 1} : ", _questionList, index),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          StreamBuilder<QuerySnapshot>(
              stream: Firestore.instance.collection("slides").snapshots(),
              builder: (BuildContext context,
                  AsyncSnapshot<QuerySnapshot> snapshot) {
                if (!snapshot.hasData) return new Text("Loading...");
                _questionList = snapshot.data.documents;
                return new Scaffold(
                  body: Stack(
                    children: <Widget>[
                      MainBackground(),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(20, 30, 20, 30),
                        child: new Container(
                          decoration: new BoxDecoration(
                              image: new DecorationImage(
                                  image: new AssetImage(
                                      "images/wooden-background.jpg"),
                                  fit: BoxFit.cover)),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(20, 30, 20, 30),
                        child: Padding(
                          padding: const EdgeInsets.only(bottom: 40.0),
                          child: ListView.builder(
                            itemBuilder: generateListItem,
                            itemCount: _quiz.length,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(0, 290, 0, 0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: <Widget>[
                            GestureDetector(
                                child: Stack(
                                  alignment: AlignmentDirectional.center,
                                  children: <Widget>[
                                    Image.asset("images/button-custom.png"),
                                    Text('Tạo bài học mới',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 20.0))
                                  ],
                                ),
                                onTap: createTest),
                            GestureDetector(
                              child: Stack(
                                alignment: AlignmentDirectional.center,
                                children: <Widget>[
                                  Image.asset("images/button-custom.png"),
                                  Text(
                                    "Thêm slide mới",
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 20.0),
                                  )
                                ],
                              ),
                              onTap: addSlide,
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                );
              }),
          StreamBuilder(
            stream: Firestore.instance.collection('custom_lessons').snapshots(),
            builder: (context, snapshot) {
              if (!snapshot.hasData) return new Text('Loading...');
              List lessons = snapshot.data.documents;
              lessonNo = lessons.length + 1;
              return Text('');
            },
          ),
          CommonButton()
        ],
      ),
    );
  }
}

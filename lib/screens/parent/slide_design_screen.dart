import 'package:flutter/material.dart';
import 'package:kid_learning/common/custom_widget.dart';
import 'package:kid_learning/models/custom_slide.dart';
import 'package:kid_learning/screens/parent/parent_selection_screen.dart';
import 'package:kid_learning/screens/parent/slide_list_screen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:toast/toast.dart';
import 'package:kid_learning/common/globals.dart' as globals;
import 'package:flutter/services.dart';

class SlideDesignScreen extends StatefulWidget {
  SlideDesignScreen(
      {Key key})
      : super(key: key);


  @override
  State<StatefulWidget> createState() => _SlideDesignScreenState();

}

class _SlideDesignScreenState extends State<SlideDesignScreen> {

  String en = "";
  String pronounce = "";
  String vi = "";
  String imageLink = "";
  String videoLink = "";
  String level = "";

  void doNothing(){}

  void addSlide() async{

    var content = Slide(vi, en, pronounce).toString();

    Firestore.instance.collection('slides').add({
      'content': {
        'vi': vi,
        'pronounce': pronounce,
        'en': en
      },
      'gif': '',
      'image': imageLink,
      'level': level,
      'topic_id': '',
      'topic_ref': '',
      'video': videoLink});

    Toast.show("Slide mới đã được thêm thành công", context, duration: Toast.LENGTH_LONG, gravity:  Toast.BOTTOM);

//    Navigator.of(context).push(
//        new MaterialPageRoute(
//            builder: (BuildContext context){
//              return new ParentSelectionScreen();
//            }
//        )
//    );

    Future.delayed(const Duration(milliseconds: 200), () {

      setState(() {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => SlideListScreen(),
          ),
        );
      });

    });

//    Navigator.of(context).push(
//        new MaterialPageRoute(
//            builder: (BuildContext context){
//              return new SlideDesignScreen();
//            }
//        )
//    );
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        SystemChrome.setEnabledSystemUIOverlays([]);
        FocusScope.of(context).requestFocus(new FocusNode());
      },
          child: Scaffold(
        body: Stack(
          children: <Widget>[
            MainBackground(),
            Padding(
              padding: const EdgeInsets.fromLTRB(20,30,20,30),
              child: new Container(
                decoration: new BoxDecoration(image: new DecorationImage(image: new AssetImage("images/wooden-background.jpg"), fit: BoxFit.cover)),
              ),
            ),
            ListView(
              children: <Widget>[
                Padding(
                    padding: const EdgeInsets.fromLTRB(10,5,30,5),
                    child:
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0,0,0,10),
                              child: GestureDetector(
                                child: Stack(
                                  alignment: AlignmentDirectional.center,
                                  children: <Widget>[
                                    Image.asset("images/header-title.png"),
                                    Text(
                                      "Tùy chỉnh slide",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(color: Colors.black, fontSize: 20.0),
                                    )
                                  ],
                                ),
                                onTap: doNothing,
                              ),
                            ),

                            Row(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(30,0,20,0),
                                  child: Image(
                                    image: new AssetImage("images/english_icon.png"),
                                    width: 45,
                                    height: 45,
                                    color: null,
                                    fit: BoxFit.fill,
                                    alignment: Alignment.center,
                                  ),
                                ),

                                Flexible(
                                  child: Container(
                                    decoration: new BoxDecoration(
                                      shape: BoxShape.rectangle,
                                      color: Color(0x00000000).withOpacity(0.5),
                                      border: new Border.all(
                                        color: Colors.white,
                                        width: 1.0,
                                      ),
                                    ),
                                    child: new TextField(
                                      onChanged: (text){
                                        en = text;
                                      },
                                      style: TextStyle(color: Colors.white, fontSize: 15.0),
                                      textAlign: TextAlign.center,
                                      decoration: new InputDecoration(
                                        hintText: 'Chữ tiếng Anh',
                                        hintStyle: TextStyle(color: Color(0xFFFFFFFF).withOpacity(0.5), fontSize: 15.0),
                                        border: InputBorder.none,

                                      ),
                                    ),
                                  )
                                ),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(30,0,20,0),
                                  child: Image(
                                    image: new AssetImage("images/vietnam_icon.png"),
                                    width: 45,
                                    height: 45,
                                    color: null,
                                    fit: BoxFit.fill,
                                    alignment: Alignment.center,
                                  ),
                                ),
                                Flexible(
                                    child: Container(
                                      decoration: new BoxDecoration(
                                        shape: BoxShape.rectangle,
                                        color: Color(0x00000000).withOpacity(0.5),
                                        border: new Border.all(
                                          color: Colors.white,
                                          width: 1.0,
                                        ),
                                      ),
                                      child: new TextField(
                                        onChanged: (text){
                                          vi = text;
                                        },
                                        style: TextStyle(color: Colors.white, fontSize: 15.0),
                                        textAlign: TextAlign.center,
                                        decoration: new InputDecoration(
                                          hintText: 'Chữ tiếng Việt',
                                          hintStyle: TextStyle(color: Color(0xFFFFFFFF).withOpacity(0.5), fontSize: 15.0),
                                          border: InputBorder.none,

                                        ),
                                      ),
                                    )
                                ),

                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0,10,0,10),
                              child: Row(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(30,0,20,0),
                                    child: Image(
                                      image: new AssetImage("images/pronounce_icon2.png"),
                                      width: 45,
                                      height: 45,
                                      color: null,
                                      fit: BoxFit.fill,
                                      alignment: Alignment.center,
                                    ),
                                  ),
                                  Flexible(
                                      child: Container(
                                        decoration: new BoxDecoration(
                                          shape: BoxShape.rectangle,
                                          color: Color(0x00000000).withOpacity(0.5),
                                          border: new Border.all(
                                            color: Colors.white,
                                            width: 1.0,
                                          ),
                                        ),
                                        child: new TextField(
                                          onChanged: (text){
                                            pronounce = text;
                                          },
                                          style: TextStyle(color: Colors.white, fontSize: 15.0),
                                          textAlign: TextAlign.center,
                                          decoration: new InputDecoration(
                                            hintText: 'Phát âm',
                                            hintStyle: TextStyle(color: Color(0xFFFFFFFF).withOpacity(0.5), fontSize: 15.0),
                                            border: InputBorder.none,

                                          ),
                                        ),
                                      )
                                  ),
                                  Padding(
                                    padding: const EdgeInsets.fromLTRB(30,0,20,0),
                                    child: Image(
                                      image: new AssetImage("images/level_icon.png"),
                                      width: 45,
                                      height: 45,
                                      color: null,
                                      fit: BoxFit.fill,
                                      alignment: Alignment.center,
                                    ),
                                  ),
                                  Flexible(
                                      child: Container(
                                        decoration: new BoxDecoration(
                                          shape: BoxShape.rectangle,
                                          color: Color(0x00000000).withOpacity(0.5),
                                          border: new Border.all(
                                            color: Colors.white,
                                            width: 1.0,
                                          ),
                                        ),
                                        child: new TextField(
                                          onChanged: (text){
                                            level = text;
                                          },
                                          style: TextStyle(color: Colors.white, fontSize: 15.0),
                                          textAlign: TextAlign.center,
                                          decoration: new InputDecoration(
                                            hintText: 'Mức độ',
                                            hintStyle: TextStyle(color: Color(0xFFFFFFFF).withOpacity(0.5), fontSize: 15.0),
                                            border: InputBorder.none,

                                          ),
                                        ),
                                      )
                                  ),


                                ],
                              ),
                            ),

                            Row(
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(30,0,20,0),
                                  child: Image(
                                    image: new AssetImage("images/image_icon.png"),
                                    width: 45,
                                    height: 45,
                                    color: null,
                                    fit: BoxFit.fill,
                                    alignment: Alignment.center,
                                  ),
                                ),
                                Flexible(
                                    child: Container(
                                      decoration: new BoxDecoration(
                                        shape: BoxShape.rectangle,
                                        color: Color(0x00000000).withOpacity(0.5),
                                        border: new Border.all(
                                          color: Colors.white,
                                          width: 1.0,
                                        ),
                                      ),
                                      child: new TextField(
                                        onChanged: (text){
                                          imageLink = text;
                                        },
                                        style: TextStyle(color: Colors.white, fontSize: 15.0),
                                        textAlign: TextAlign.center,
                                        decoration: new InputDecoration(
                                          hintText: 'Hình ảnh',
                                          hintStyle: TextStyle(color: Color(0xFFFFFFFF).withOpacity(0.5), fontSize: 15.0),
                                          border: InputBorder.none,

                                        ),
                                      ),
                                    )
                                ),
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(30,0,20,0),
                                  child: Image(
                                    image: new AssetImage("images/video_icon.png"),
                                    width: 45,
                                    height: 45,
                                    color: null,
                                    fit: BoxFit.fill,
                                    alignment: Alignment.center,
                                  ),
                                ),
                                Flexible(
                                    child: Container(
                                      decoration: new BoxDecoration(
                                        shape: BoxShape.rectangle,
                                        color: Color(0x00000000).withOpacity(0.5),
                                        border: new Border.all(
                                          color: Colors.white,
                                          width: 1.0,
                                        ),
                                      ),
                                      child: new TextField(
                                        onChanged: (text){
                                          videoLink = text;
                                        },
                                        style: TextStyle(color: Colors.white, fontSize: 15.0),
                                        textAlign: TextAlign.center,
                                        decoration: new InputDecoration(
                                          hintText: 'Video',
                                          hintStyle: TextStyle(color: Color(0xFFFFFFFF).withOpacity(0.5), fontSize: 15.0),
                                          border: InputBorder.none,

                                        ),
                                      ),
                                    )
                                ),

                              ],
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(0,15,0,0),
                              child: GestureDetector(
                                child: Stack(
                                  alignment: AlignmentDirectional.center,
                                  children: <Widget>[
                                    Image.asset("images/button-easy.png"),
                                    Text(
                                      "Thêm slide",
                                      textAlign: TextAlign.center,
                                      style: TextStyle(color: Colors.white, fontSize: 20.0),
                                    )
                                  ],
                                ),
                                onTap: (){
                                  setState(() {
                                    globals.playBtnSound(1);
                                    addSlide();

                                    en = "";
                                    pronounce = "";
                                    vi = "";
                                    imageLink = "";
                                    videoLink = "";
                                    level = "";
                                  });
                                },
                              ),
                            ),

                          ],
                        )
                ),
              ],
            ),
            CommonButton(),
          ],
        ),
      ),
    );
  }
}
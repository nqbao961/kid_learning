import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:kid_learning/common/globals.dart' as globals;
import 'package:kid_learning/screens/parent/slide_design_screen.dart';
import 'package:kid_learning/screens/parent/lesson_design_screen.dart';
import 'package:kid_learning/screens/parent/parent_control_screen.dart';
import 'package:kid_learning/screens/parent/slide_list_screen.dart';
import 'package:kid_learning/screens/parent/lesson_list_screen.dart';
import 'package:kid_learning/common/custom_widget.dart';

class ParentSelectionScreen extends StatefulWidget {
  ParentSelectionScreen(
      {Key key})
      : super(key: key);


  @override
  State<StatefulWidget> createState() => _ParentSelectionScreenState();

}

class _ParentSelectionScreenState extends State<ParentSelectionScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          MainBackground(),
          CommonButton(),
          Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[

//                  GestureDetector(
//                    onTap: () {
//                      globals.playBtnSound(1);
//                      Navigator.push(
//                        context,
//                        MaterialPageRoute(
//                          builder: (context) => ParentControlScreen(),
//                        ),
//                      );
//                    },
//                    child: Stack(
//                      alignment: AlignmentDirectional.center,
//                      children: <Widget>[
//                        Image.asset(
//                          'images/button-blue.png',
//                          width: 160.0,
//                        ),
//                        Text(
//                          'Quản lý',
//                          textAlign: TextAlign.center,
//                          style: TextStyle(color: Colors.white, fontSize: 16.0),
//                        ),
//                      ],
//                    ),
//                  ),

                  Padding(
                    padding: const EdgeInsets.fromLTRB(0,30,0,30),
                    child: GestureDetector(
                      onTap: () {
                        globals.playBtnSound(1);
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => SlideListScreen(),
                          ),
                        );
                      },
                      child: Stack(
                        alignment: AlignmentDirectional.center,
                        children: <Widget>[
                          Image.asset(
                            'images/button-blue.png',
                            width: 160.0,
                          ),
                          Text(
                            'Thiết kế slide',
                            textAlign: TextAlign.center,
                            style: TextStyle(color: Colors.white, fontSize: 16.0),
                          ),
                        ],
                      ),
                    ),
                  ),



                  GestureDetector(
                    onTap: () {
                      globals.playBtnSound(1);
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => LessonListScreen(),
                        ),
                      );
                    },
                    child: Stack(
                      alignment: AlignmentDirectional.center,
                      children: <Widget>[
                        Image.asset(
                          'images/button-blue.png',
                          width: 160.0,
                        ),
                        Text(
                          'Thiết kế bài học',
                          textAlign: TextAlign.center,
                          style: TextStyle(color: Colors.white, fontSize: 16.0),
                        ),
                      ],
                    ),
                  ),



                ],
              )
          )
        ],
      ),
    );
  }
}
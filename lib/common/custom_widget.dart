import 'package:flutter/material.dart';
import 'package:kid_learning/screens/lesson/choose_topic_screen.dart';
import 'package:kid_learning/screens/lesson/choose_lesson_screen.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:audioplayers/audioplayers.dart';
import 'package:kid_learning/common/globals.dart' as globals;
import 'package:flutter_tts/flutter_tts.dart';
import 'package:kid_learning/screens/update_info_screen.dart';
import 'package:kid_learning/screens/login_signup_screen.dart';
import 'package:kid_learning/screens/lesson/custom_lesson_screen.dart';
import 'package:scoped_model/scoped_model.dart';
import 'package:kid_learning/models/app_state.dart';
import 'package:flutter/services.dart';
import 'package:kid_learning/screens/root_screen.dart';
import 'package:kid_learning/services/authentication.dart';
import 'package:kid_learning/screens/achievement_screen.dart';

class MainBackground extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
      decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage("images/main-background.jpg"),
              fit: BoxFit.cover)),
    ));
  }
}

class LevelButton extends StatelessWidget {
  final String text, image, userId, level;

  LevelButton(this.text, this.image, this.userId, this.level);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        globals.playBtnSound(2);
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ChooseTopicScreen(
                  userId: userId,
                  level: level,
                ),
          ),
        );
      },
      child: Stack(
        alignment: AlignmentDirectional.center,
        children: <Widget>[
          Image.asset(
            image,
          ),
          Text(
            text,
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white, fontSize: 20.0),
          ),
        ],
      ),
    );
  }
}

class CustomLessonButton extends StatelessWidget {
  final String text, image, userId;

  CustomLessonButton(this.text, this.image, this.userId);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        globals.playBtnSound(2);
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => CustomLessonScreen(userId: userId),
          ),
        );
      },
      child: Stack(
        alignment: AlignmentDirectional.center,
        children: <Widget>[
          Image.asset(
            image,
          ),
          Text(
            text,
            textAlign: TextAlign.center,
            style: TextStyle(color: Colors.white, fontSize: 20.0),
          ),
        ],
      ),
    );
  }
}

class TopicItem extends StatelessWidget {
  final String userId, textEn, textVi, imageUrl, level, topicId;
  final DocumentReference topic;
  TopicItem(this.userId, this.textEn, this.textVi, this.imageUrl, this.level,
      this.topic, this.topicId);
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 50.0),
      child: GestureDetector(
        onTap: () {
          globals.playBtnSound(1);
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ChooseLessonScreen(
                    userId: userId,
                    level: level,
                    textEn: textEn,
                    topic: topic,
                    topicId: topicId,
                  ),
            ),
          );
        },
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            FadeInImage.assetNetwork(
              placeholder: 'images/loading.gif',
              image: imageUrl,
              width: 130.0,
              height: 130.0,
            ),
            Stack(
              alignment: AlignmentDirectional.bottomCenter,
              children: <Widget>[
                Container(
                    width: 150,
                    height: 100,
                    child: Image.asset('images/wooden-frame2.png', fit: BoxFit.fill,)
                ),
                Container(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        textEn,
                        style:
                        TextStyle(fontSize: 20.0, color: Colors.black, shadows: [
                          Shadow(
                              offset: Offset(-1.5, -1.5),
                              color: Colors.white),
                          Shadow(
                              offset: Offset(1.5, -1.5),
                              color: Colors.white),
                          Shadow(
                              offset: Offset(1.5, 1.5),
                              color: Colors.white),
                          Shadow(
                              offset: Offset(-1.5, 1.5),
                              color: Colors.white),
                        ]),
                      ),
                      Text(
                        textVi,
                        style:
                        TextStyle(fontSize: 20.0, color: Colors.black, shadows: [
                          Shadow(
                              offset: Offset(-1.5, -1.5),
                              color: Colors.white),
                          Shadow(
                              offset: Offset(1.5, -1.5),
                              color: Colors.white),
                          Shadow(
                              offset: Offset(1.5, 1.5),
                              color: Colors.white),
                          Shadow(
                              offset: Offset(-1.5, 1.5),
                              color: Colors.white),
                        ]),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class CommonButton extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CommonButtonState();
}

class _CommonButtonState extends State<CommonButton> {
  final FlutterTts flutterTts = FlutterTts();
  String _voiceType;

  @override
  void initState() {
    super.initState();
    _voiceType = globals.voiceType;
  }

  @override
  Widget build(BuildContext context) {
    _updateInfoButton() {
      globals.playBtnSound(1);
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => UpdateInfoScreen(
                userId: globals.userId,
                auth: globals.auth,
              ),
        ),
      );
    }

    _signOut() async {
      globals.playBtnSound(1);
      globals.stopBackgroundMusic();
      try {
        await globals.auth.signOut();
        globals.onSignedOut();
        Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => RootScreen(auth: new Auth())),
          ModalRoute.withName('/main'),
        );
      } catch (e) {
        print(e);
      }
    }

    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Padding(
          padding: const EdgeInsets.all(5.0),
          child: GestureDetector(
            child: Image.asset(
              'images/back.png',
              width: 40.0,
            ),
            onTap: () {
              globals.playBtnSound(1);
              SystemChrome.setEnabledSystemUIOverlays([]);
              FocusScope.of(context).requestFocus(new FocusNode());
              Navigator.pop(context, true);
            },
          ),
        ),
        Row(
          children: <Widget>[
            Padding(
                padding: const EdgeInsets.all(5.0),
                child: GestureDetector(
                  child: Image.asset(
                    'images/trophy-cup.png',
                    width: 40.0,
                    height: 40.0,
                  ),
                  onTap: () {
                    globals.playBtnSound(1);
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => AchievementScreen(),
                      ),
                    );
                  },
                )),
            ScopedModelDescendant<AppStateModel>(
                builder: (context, child, model) {
              return Padding(
                padding: const EdgeInsets.all(5.0),
                child: GestureDetector(
                  child: Image.asset(
                    model.volumeImg,
                    width: 40.0,
                  ),
                  onTap: () {
                    globals.resumePauseBackgroundMusic();
                    if (globals.volume == 0.0) {
                      globals.volume = 1.0;
                      model.toggleVolumeImg();
                    } else {
                      globals.playBtnSound(1);
                      globals.volume = 0.0;
                      //globals.stopBackgroundMusic();
                      model.toggleVolumeImg();
                    }
                    flutterTts.setVolume(globals.volume);
                  },
                ),
              );
            }),
            Padding(
              padding: const EdgeInsets.all(5.0),
              child: GestureDetector(
                child: Image.asset(
                  'images/setting.png',
                  width: 40.0,
                ),
                onTap: () {
                  globals.playBtnSound(1);
                  showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return StatefulBuilder(builder: (context, setState) {
                          void _handleVoiceType(String value) {
                            globals.voiceType = value;
                            setState(() {
                              _voiceType = value;
                            });
                          }

                          return SimpleDialog(
                            title: Text('Setting'),
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Text(
                                    'Giọng đọc: ',
                                    style: TextStyle(fontSize: 17.0),
                                  ),
                                  DropdownButton<String>(
                                    items: [
                                      DropdownMenuItem<String>(
                                        value: "en-us-x-sfg#female_1-local",
                                        child: Text(
                                          "Nữ Anh-Mỹ",
                                        ),
                                      ),
                                      DropdownMenuItem<String>(
                                        value: "en-us-x-sfg#male_1-local",
                                        child: Text(
                                          "Nam Anh-Mỹ",
                                        ),
                                      ),
                                      DropdownMenuItem<String>(
                                        value: "en-gb-x-fis#female_1-local",
                                        child: Text(
                                          "Nữ Anh-Anh",
                                        ),
                                      ),
                                      DropdownMenuItem<String>(
                                        value: "en-gb-x-fis#male_1-local",
                                        child: Text(
                                          "Nam Anh-Anh",
                                        ),
                                      ),
                                    ],
                                    onChanged: _handleVoiceType,
                                    value: _voiceType,
                                  ),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.only(bottom: 8.0),
                                child: GestureDetector(
                                  onTap: _updateInfoButton,
                                  child: Stack(
                                    alignment: AlignmentDirectional.center,
                                    children: <Widget>[
                                      Image.asset(
                                        'images/button-blue.png',
                                        width: 160.0,
                                      ),
                                      Text(
                                        'Thông tin',
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 16.0),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                              GestureDetector(
                                onTap: _signOut,
                                child: Stack(
                                  alignment: AlignmentDirectional.center,
                                  children: <Widget>[
                                    Image.asset(
                                      'images/button-red.png',
                                      width: 160.0,
                                    ),
                                    Text(
                                      'Đăng xuất',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 16.0),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          );
                        });
                      });
                },
              ),
            ),
          ],
        )
      ],
    );
  }
}

class SettingDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[Text('data')],
      ),
    );
  }
}

library my_globals;

import 'package:audioplayers/audioplayers.dart';
import 'package:kid_learning/services/authentication.dart';
import 'package:flutter/material.dart';
import 'package:audioplayers/audio_cache.dart';

String userId = '';
String email = '';
double volume = 1.0;
BaseAuth auth;
VoidCallback onSignedOut;
VoidCallback onSignedIn;
String voiceType = 'en-us-x-sfg#female_1-local';

AudioPlayer audioPlayer = new AudioPlayer();
AudioCache backgroundMusic = new AudioCache(fixedPlayer: audioPlayer);
bool isBackgroundMusicPlaying = false;
bool isBackgroundMusicPaused = false;

void playBackgroundMusic() {
  const musicPath = "sounds/theme_music.mp3";
  if (!isBackgroundMusicPlaying) {
    backgroundMusic.loop(musicPath, volume: volume / 2);
    isBackgroundMusicPlaying = true;
  }
}

void resumePauseBackgroundMusic() {
  if (isBackgroundMusicPaused) {
    isBackgroundMusicPaused = false;
    audioPlayer.resume();
  } else {
    isBackgroundMusicPaused = true;
    audioPlayer.pause();
  }
}

// Only uses these function when study and do exam
void pauseBackgroundMusic() {
  audioPlayer.pause();
}

void resumeBackgroundMusic() {
  if (volume == 0.0) {
  } else {
    audioPlayer.resume();
  }
}
//---------

void stopBackgroundMusic() {
  audioPlayer.stop();
  audioPlayer.release();
  isBackgroundMusicPlaying = false;
}

void playBtnSound(int choice) {
  AudioCache btnSound = new AudioCache();
  String btnSoundPath;
  switch (choice) {
    case 1:
      btnSoundPath = 'sounds/btn_sound.wav';
      break;
    case 2:
      btnSoundPath = 'sounds/btn_sound2.wav';
      break;
    default:
      btnSoundPath = 'sounds/btn_sound.wav';
      break;
  }
  btnSound.play(btnSoundPath, volume: volume);
}

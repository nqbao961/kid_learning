import 'package:scoped_model/scoped_model.dart';

class AppStateModel extends Model {
  String _volumeImg = 'images/volume.png';

  String get volumeImg => _volumeImg;

  void toggleVolumeImg() {
    if (_volumeImg == 'images/volume.png') {
      _volumeImg = 'images/volume-mute.png';
    } else {
      _volumeImg = 'images/volume.png';
    }
    notifyListeners();
  }
}

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Slide {
  String vi = "";
  String en = "";
  String pronounce = "";

  Slide(String vi, String en, String pro) {
    this.vi = vi;
    this.en = en;
    this.pronounce = pro;
  }
  String toString(){
    return '{en: "$en", pronounce: "$pronounce", vi: "$vi"}';
  }
}
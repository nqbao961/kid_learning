import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class Topic {
  final String title;
  final String thubnailUrl;

  final DocumentReference reference;

 Topic.fromMap(Map<String, dynamic> map, {this.reference})
     : assert(map['title'] != null),
       assert(map['thubnail'] != null),
       title = map['title'],
       thubnailUrl = map['thubnail'];

 Topic.fromSnapshot(DocumentSnapshot snapshot)
     : this.fromMap(snapshot.data, reference: snapshot.reference);

 @override
 String toString() => "Topic<$title:$thubnailUrl>";
}
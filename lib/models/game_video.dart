import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

class GameVideo {
  String gifUrl = "";
  String title = "";

  GameVideo(String gifUrl, String title) {
    this.gifUrl = gifUrl;
    this.title = title;
  }
}
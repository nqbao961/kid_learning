import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:kid_learning/services/authentication.dart';
import 'package:kid_learning/screens/root_screen.dart';
import 'package:video_player/video_player.dart';
import 'package:audioplayers/audio_cache.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kid_learning/common/globals.dart' as globals;
import 'package:scoped_model/scoped_model.dart';
import 'package:kid_learning/models/app_state.dart';
// import 'package:kid_learning/screens/choose_level_screen.dart';
// import 'package:kid_learning/screens/choose_topic_screen.dart';
// import 'package:kid_learning/screens/login_signup_screen.dart';

void main() => runApp(MyApp(model: AppStateModel()));

class MyApp extends StatelessWidget {
  final AppStateModel model;

  const MyApp({Key key, this.model}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final appName = 'Kid Learning';
    SystemChrome.setEnabledSystemUIOverlays([]);
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.landscapeRight,
      DeviceOrientation.landscapeLeft,
    ]);
    return ScopedModel<AppStateModel>(
      model: model,
      child: MaterialApp(
        title: appName,
        debugShowCheckedModeBanner: false,
        home: Builder(builder: (context) => new SplashVideo()),
        routes: {
          '/main': (context) => RootScreen(auth: new Auth()),
          // '/login-signup': (context) => LoginSignUpScreen(),
          // '/choose-level': (context) => ChooseLevelScreen(),
          // '/choose-topic': (context) => ChooseTopicScreen(),
        },
        theme: ThemeData(fontFamily: 'Pony'),
      ),
    );
  }
}

// --------------- Video for splash screen

class SplashVideo extends StatefulWidget {
  @override
  SplashVideoState createState() => new SplashVideoState();
}

// --------------- Animation for splash screen

class SplashAnimation extends StatefulWidget {
  @override
  SplashAnimationState createState() => new SplashAnimationState();
}

// -----------------------------------------

class SplashVideoState extends State<SplashVideo> {
  VideoPlayerController _controller;
  Future<void> _initializeVideoPlayerFuture;
  AudioCache soundController = new AudioCache();

  startTimeout() async {
    var duration = const Duration(seconds: 6);
    return new Timer(duration, handleTimeout);
  }

  void handleTimeout() {
    Navigator.pushReplacementNamed(context, "/main");
  }

  void playSound() {
    const alarmAudioPath = "sounds/splash_sound.wav";
    soundController.play(alarmAudioPath, volume: globals.volume);
  }

  @override
  void initState() {
    playSound();
    // Create and store the VideoPlayerController. The VideoPlayerController
    // offers several different constructors to play videos from assets, files,
    // or the internet.
    _controller = VideoPlayerController.asset('assets/sounds/splash.mp4');

    // Initialize the controller and store the Future for later use
    _initializeVideoPlayerFuture = _controller.initialize();

    // Use the controller to loop the video
    _controller.setLooping(true);
    _controller.play();

    super.initState();
    startTimeout();
  }

  @override
  void dispose() {
    // Ensure you dispose the VideoPlayerController to free up resources
    _controller.dispose();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // Use a FutureBuilder to display a loading spinner while you wait for the
      // VideoPlayerController to finish initializing.
      body: FutureBuilder(
        future: _initializeVideoPlayerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return GestureDetector(
              onTap: handleTimeout,
              child: AspectRatio(
//              aspectRatio: _controller.value.aspectRatio,
                aspectRatio: MediaQuery.of(context).size.width /
                    MediaQuery.of(context).size.height,
                child: VideoPlayer(_controller),
              ),
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
//      floatingActionButton: FloatingActionButton(
//        onPressed: () {
//          // Wrap the play or pause in a call to `setState`. This ensures the
//          // correct icon is shown
//          setState(() {
//            // If the video is playing, pause it.
//            if (_controller.value.isPlaying) {
//              _controller.pause();
//            } else {
//              // If the video is paused, play it
//              _controller.play();
//            }
//          });
//        },
//        // Display the correct icon depending on the state of the player.
//        child: Icon(
//          _controller.value.isPlaying ? Icons.pause : Icons.play_arrow,
//        ),
//      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}

class SplashAnimationState extends State<SplashAnimation>
    with SingleTickerProviderStateMixin {
  var _iconAnimationController;
  var _iconAnimation;

  startTimeout() async {
    var duration = const Duration(seconds: 3);
    return new Timer(duration, handleTimeout);
  }

  void handleTimeout() {
    Navigator.pushReplacementNamed(context, "/main");
  }

  @override
  void initState() {
    super.initState();

    _iconAnimationController = new AnimationController(
        vsync: this, duration: new Duration(milliseconds: 6000));

    _iconAnimation = new CurvedAnimation(
        parent: _iconAnimationController, curve: Curves.bounceIn);
    _iconAnimation.addListener(() => this.setState(() {}));

    _iconAnimationController.forward();

    startTimeout();
  }

  @override
  Widget build(BuildContext context) {
    return new Center(
        child: new Image(
      image: new AssetImage("images/logo.png"),
      width: _iconAnimation.value * 100,
      height: _iconAnimation.value * 100,
    ));
  }
}

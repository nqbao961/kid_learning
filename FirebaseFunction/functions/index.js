
// Create and Deploy Your First Cloud Functions
// https://firebase.google.com/docs/functions/write-firebase-functions


const functions = require('firebase-functions');
const admin = require('firebase-admin');
admin.initializeApp();

function getRandom(arr, n) {
    var result = new Array(n),
        len = arr.length,
        taken = new Array(len);
    if (n > len)
        throw new RangeError("getRandom: more elements taken than available");
    while (n--) {
        var x = Math.floor(Math.random() * len);
        result[n] = arr[x in taken ? taken[x] : x];
        taken[x] = --len in taken ? taken[len] : len;
    }
    return result;
}

// exports.getRecommendQuizList = functions.https.onRequest((request, response) => {
	// var ref = admin.firestore().collection('quizzes');
	// var recommendQuizList = ref.where(firebase.firestore.FieldPath.documentId(), '==', 'quiz_easy_animals_1');
 // 	response.send(recommendQuizList);

// });

exports.getRecommendedQuizList = functions.https.onCall((data, context) => {
    const level = data.level;
    const userId = data.userId;
    return admin.firestore()
        .collection('quizzes')
        .where('level', '==', level)
        .get().then(querySnapshot => {
            var result = [];
            var realScoreList = [];
            var scoreList = querySnapshot.docs.map((doc, index) => {
                var totalResult = doc.data().total_result;
                var personalResult = doc.data().results[userId];

                var realScore;
                if(totalResult.correct_num !== 0) {
                    var totalScore = totalResult.incorrect_num/totalResult.correct_num;
                    if(personalResult.correct_num !== 0) {
                        var personalScore = personalResult.incorrect_num/personalResult.correct_num;
                        realScore = (totalScore*50/100) + (personalScore*50/100);
                    }
                    else realScore = totalScore;
                } else realScore = 0;

                realScoreList.push(new scoreObject(realScore, index));
            });

            var shuffleScoreList = shuffle(realScoreList);
            var sortedScoreList = shuffleScoreList.sort((a,b) => (a.score > b.score) ? 1: -1).slice(0, 10);
            //var docs = sortedScoreList.map((function (score){

                //var position = search(score, scoreList);
                //return querySnapshot.docs[position];
            //}));

            var docs = sortedScoreList.map((scoreObject) => {
                //print("Chosen: "+scoreObject.pos + " : " + scoreObject.score);
                return querySnapshot.docs[scoreObject.pos];
            });

            docs.forEach((doc) => {
                result.push(admin.firestore().doc(doc.ref.path));
            })
            return admin.firestore()
                .collection('users').doc(userId).update({ recommended_quiz_list: result });
        })
});

class scoreObject  {
  constructor(score, pos) {
    this.score = score;
    this.pos = pos;
  }
}

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  while (0 !== currentIndex) {

    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

function search(nameKey, myArray){
    for (var i=0; i < myArray.length; i++) {
        if (myArray[i].name === nameKey) {
            return i;
        }
    }
}

exports.getQuizList = functions.https.onCall((data, context) => {
    const level = data.level;
    const topic = data.topic;
    const userId = data.userId;
    return admin.firestore()
        .collection('quizzes')
        .where('level', '==', level)
        .where('topic_id', '==', topic)
        .get().then(querySnapshot => {
            var result = [];
            var docs = getRandom(querySnapshot.docs, 3);
            docs.forEach((doc) => {
                result.push(admin.firestore().doc(doc.ref.path));
            })
            return admin.firestore()
                .collection('users').doc(userId).update({ quizList: result });
        })
});